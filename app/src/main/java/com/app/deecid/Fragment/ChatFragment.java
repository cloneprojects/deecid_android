package com.app.deecid.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.deecid.Activity.SuperLikesActivity;
import com.app.deecid.Adapters.ChatRoomAdapter;
import com.app.deecid.Adapters.NewMatchesAdapter;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.Helper.Utils;
import com.app.deecid.R;
import com.app.deecid.Services.MessageEvent;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.VolleyCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment {
    private boolean superLikes;
    private JSONArray mJsonArray;
    private String count;
    private String superLikeUrl;
    private CircleImageView userImage;
    private ImageView isSuperLiked;
    private TextView userName;
    RecyclerView newMatches;
    FrameLayout frame;
    RecyclerView chatRooms;
    JSONArray newMatchesArray = new JSONArray();
    JSONArray chatRoomArray = new JSONArray();
    AppSettings appSettings;
    EditText searchBox;
    LinearLayout emptyLayout, newMatchesLayout;
    TextView messagesHeading;
    LinearLayout searchLayout, seeWhoLiked;
    private ChatRoomAdapter chatRoomAdapter;
    private NewMatchesAdapter newMatchesAdapter;
    private String TAG = ChatFragment.class.getSimpleName();

    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        appSettings = new AppSettings(getActivity());
        initViews(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getVAlues();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Utils.hideKeyboard(getActivity());

    }

    public void getVAlues() {
        try {
            getNewMatchesDatas();
//            getChatRoomDatas();
//            getChatLists();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        loadList();
    }

    public void getNewMatchesDatas() throws JSONException {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.VIEW_MATCHES, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d(TAG, "onSuccess: " + response);
                    chatRoomArray = new JSONArray();
                    newMatchesArray = new JSONArray();
                    if (response.has("results")) {
                        JSONArray valui = response.optJSONArray("results");
                        for (int i = 0; i < valui.length(); i++) {
                            JSONObject jsonObject1 = valui.optJSONObject(i);
                            String json = jsonObject1.optString("lastMsg");
                            if (!json.equalsIgnoreCase("null")) {
                                chatRoomArray.put(jsonObject1);

                            } else {

                                newMatchesArray.put(jsonObject1);

                            }
                        }


                        if (newMatchesArray.length() > 0) {
                            newMatchesLayout.setVisibility(View.VISIBLE);


                            newMatchesAdapter = new NewMatchesAdapter(newMatchesArray, getActivity());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            newMatches.setLayoutManager(linearLayoutManager);
                            newMatches.setAdapter(newMatchesAdapter);
                            newMatchesAdapter.notifyDataSetChanged();

                        } else {
                            newMatchesLayout.setVisibility(View.GONE);
                        }
                        if (chatRoomArray.length() > 0) {
                            emptyLayout.setVisibility(View.GONE);
                            chatRooms.setVisibility(View.VISIBLE);

                            messagesHeading.setVisibility(View.VISIBLE);
                            chatRoomAdapter = new ChatRoomAdapter(chatRoomArray, getActivity());
                            LinearLayoutManager linearLayoutManagers = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            chatRooms.setLayoutManager(linearLayoutManagers);
                            chatRooms.setAdapter(chatRoomAdapter);
                            chatRoomAdapter.notifyDataSetChanged();
                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(chatRooms.getContext(), linearLayoutManagers.getOrientation());
                            chatRooms.addItemDecoration(dividerItemDecoration);
                        } else {
                            emptyLayout.setVisibility(View.VISIBLE);
                            chatRooms.setVisibility(View.GONE);
                            messagesHeading.setVisibility(View.GONE);
                        }

                        if (newMatchesArray.length() == 0 && chatRoomArray.length() == 0) {
                            searchLayout.setVisibility(View.GONE);
                        } else {
                            searchLayout.setVisibility(View.VISIBLE);

                        }
                    } else {
                        newMatchesLayout.setVisibility(View.GONE);
                        searchLayout.setVisibility(View.GONE);
                        chatRooms.setVisibility(View.GONE);
                        messagesHeading.setVisibility(View.GONE);
                        emptyLayout.setVisibility(View.VISIBLE);

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getChatLists() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethod(getActivity(), UrlHelper.CHAT_LISTS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });


    }


    private void getChatRoomDatas() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userName", "Brad Pit");
        jsonObject.put("lastMsg", "Hello");
        jsonObject.put("receiverId", "1");
        jsonObject.put("displayPic", "http://img.etonline.com/1242911076001/201703/2920/1242911076001_5367624651001_et-bradpittmotorcycle-032117-hulu.jpg?pubId=1242911076001");
        chatRoomArray.put(jsonObject);

        jsonObject = new JSONObject();
        jsonObject.put("userName", "Priyanka Chopra");
        jsonObject.put("lastMsg", "Hi Can we meet?");
        jsonObject.put("receiverId", "1");

        jsonObject.put("displayPic", "https://images-na.ssl-images-amazon.com/images/M/MV5BMjAxNzUwNjExOV5BMl5BanBnXkFtZTcwNDUyMTUxNw@@._V1_UY1200_CR488,0,630,1200_AL_.jpg");
        chatRoomArray.put(jsonObject);

        jsonObject = new JSONObject();
        jsonObject.put("userName", "Anna Hathway");
        jsonObject.put("lastMsg", "Yes, How about you?");
        jsonObject.put("receiverId", "1");

        jsonObject.put("displayPic", "http://astrokentico.s3.amazonaws.com/rojakdaily/media/jessica-chua/entertainment/2017/july/anne%20hathaway%20might%20be%20your%20new%20barbie/anne-hathaway.png?ext=.png");
        chatRoomArray.put(jsonObject);

        jsonObject = new JSONObject();
        jsonObject.put("userName", "Eva Mendes");
        jsonObject.put("lastMsg", "Will be Back Soon.");
        jsonObject.put("receiverId", "1");

        jsonObject.put("displayPic", "https://pmcdeadline2.files.wordpress.com/2011/06/evam_20110614180129.jpg");
        chatRoomArray.put(jsonObject);


        chatRoomAdapter = new ChatRoomAdapter(chatRoomArray, getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        chatRooms.setLayoutManager(linearLayoutManager);
        chatRooms.setAdapter(chatRoomAdapter);
        chatRoomAdapter.notifyDataSetChanged();
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(chatRooms.getContext(), linearLayoutManager.getOrientation());
        chatRooms.addItemDecoration(dividerItemDecoration);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        try {
            getNewMatchesDatas();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void loadList() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("accessToken", appSettings.getAccessToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.SEE_LIKED_LIST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.e(TAG, "onSuccess: " + response);
                //mJsonArray = response.optJSONArray("LikedPerson");
                //superLikes = mJsonArray.length() != 0;
                // Log.e(TAG, "onSuccess: " + mJsonArray.length());
                Log.e(TAG, "onSuccess: " + superLikes);
                if (superLikes) {
                    seeWhoLiked.setVisibility(View.VISIBLE);
                    count = String.valueOf(mJsonArray.length());
                    superLikeUrl = mJsonArray.optJSONObject(0).optString("displayPic");

                    if (appSettings.getUserType().equalsIgnoreCase("gold")) {
                        userName.setText(count);
                        if (superLikeUrl != null) {
                            Glide.with(getActivity())
                                    .load(superLikeUrl)
                                    .into(userImage);
                        }
                    } else {
                        userName.setText("");
                        if (superLikeUrl != null) {
                            Glide.with(getActivity())
                                    .load(superLikeUrl)
                                    .override(10, 10)
                                    .into(userImage);
                        }
                    }

                } else {
                    seeWhoLiked.setVisibility(View.GONE);
                    //userImage.setImageResource();
                    //userName.setText("");
                }
            }
        });
    }

    private void initViews(View view) {
        newMatches = (RecyclerView) view.findViewById(R.id.newMatches);
        frame = view.findViewById(R.id.frame);
        userName = view.findViewById(R.id.userName);
        userImage = view.findViewById(R.id.userImage);
        isSuperLiked = view.findViewById(R.id.isSuperLiked);

        //super likes count
        //loadList();

        Log.e(TAG, "usertype: " + appSettings.getUserType());
        Log.e(TAG, "usertype: " + superLikes);


        frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appSettings.getUserType().equalsIgnoreCase("gold")) {

                    if (superLikes) {
                        Log.e(TAG, "onClick: ");
                        startActivity(new Intent(getActivity(), SuperLikesActivity.class));
                    }
                }

            }
        });

        messagesHeading = (TextView) view.findViewById(R.id.messagesHeading);
        chatRooms = (RecyclerView) view.findViewById(R.id.chatRooms);
        searchBox = (EditText) view.findViewById(R.id.searchBox);
        emptyLayout = (LinearLayout) view.findViewById(R.id.emptyLayout);
        searchLayout = (LinearLayout) view.findViewById(R.id.searchLayout);
        seeWhoLiked = (LinearLayout) view.findViewById(R.id.seeWhoLiked);
        newMatchesLayout = (LinearLayout) view.findViewById(R.id.newMatchesLayout);
        searchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                performSearch();


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void performSearch() {
        String newText = searchBox.getText().toString();
        Log.e(TAG, "String: " + newText);
        try {
            // Log.e(TAG, "Edittext: " + searchBox.getText().toString());
            newMatchesAdapter.getFilter().filter(newText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            chatRoomAdapter.getFilter().filter(newText);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
