package com.app.deecid.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.deecid.Activity.EditProfileActivity;
import com.app.deecid.Activity.MyProfile;
import com.app.deecid.Activity.SettingsActivity;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.Helper.Utils;
import com.app.deecid.R;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.VolleyCallback;
import com.rd.PageIndicatorView;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.view.PaymentMethodsActivity;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {
    public static CircleImageView profileImage;
    public static TextView jobDetails;
    public static TextView nameDetails;
    LinearLayout editProfile;
    LinearLayout settings;
    CardView cardBackgroundOne, cardBackgroundTwo, cardBackgroundThree;
    FrameLayout firstImage, secondImage;
    LinearLayout topOne, topTwo, topThree;
    LinearLayout parentOne, parentTwo, parentThree;
    ViewPager ViewPager;
    CardView tindoPlus;
    ImageView gradientBg;
    int currentitem = 0;
    AppSettings appSettings;
    Button continueButton;
    TextView dismissButton;
    String[] titles = new String[6];
    String[] subtitles = new String[6];
    int[] icons = new int[]{R.drawable.dialog_location, R.drawable.fire_icon, R.drawable.dialog_key, R.drawable.dialog_switch, R.drawable.heart_icon, R.drawable.forward_icon, R.drawable.dialog_stop};
    private String TAG = ProfileFragment.class.getSimpleName();
    private PageIndicatorView circleIndicator;
    private String cardID;


    public ProfileFragment() {
        // Required empty public constructor
    }

    public static void setData(final Context context) {
        Log.d("asdasd", "setData: ");
        final AppSettings appSettings = new AppSettings(context);
        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    Glide.with(context).load(appSettings.getUserImageUrl()).into(profileImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!appSettings.getCompany().trim().equalsIgnoreCase("")) {
                    jobDetails.setText(appSettings.getJobTitle() + " at " + appSettings.getCompany());
                } else if (!appSettings.getJobTitle().trim().equalsIgnoreCase("")) {
                    jobDetails.setText(appSettings.getJobTitle());

                } else {
                    jobDetails.setText("");
                }

                nameDetails.setText(appSettings.getUserName() + "," + appSettings.getAge());
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        PaymentConfiguration.init(getActivity().getResources().getString(R.string.stripe_key));

        initViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Utils.hideKeyboard(getActivity());

    }

    private void initViews(View view) {
        appSettings = new AppSettings(getActivity());
        titles = new String[]{getActivity().getResources().getString(R.string.swipe_around_the), getActivity().getResources().getString(R.string.get_free), getActivity().getResources().getString(R.string.choose_who_sees_you), getActivity().getResources().getString(R.string.control_your_profile), getActivity().getResources().getString(R.string.unlimited_likes), getActivity().getResources().getString(R.string.unlimited_rewinds), getActivity().getResources().getString(R.string.turn_off)};
        subtitles = new String[]{getActivity().getResources().getString(R.string.passport_to), getActivity().getResources().getString(R.string.skip_the_line), getActivity().getResources().getString(R.string.only_be_shown), getActivity().getResources().getString(R.string.limit_what_others), getActivity().getResources().getString(R.string.swipe_right_as), getActivity().getResources().getString(R.string.go_back_and), getActivity().getResources().getString(R.string.have_fun)};
        Log.d(TAG, "initViews: " + titles.length + "," + subtitles.length + "," + icons.length);
        editProfile = (LinearLayout) view.findViewById(R.id.editProfile);
        profileImage = (CircleImageView) view.findViewById(R.id.profileImage);
        tindoPlus = view.findViewById(R.id.tindoPlus);

        Log.e(TAG, "usertype: " + appSettings.getUserType());
        if (!appSettings.getUserType().equalsIgnoreCase("basic")) {
            tindoPlus.setVisibility(View.GONE);
        } else {
            tindoPlus.setVisibility(View.VISIBLE);
        }

        settings = (LinearLayout) view.findViewById(R.id.settings);
        jobDetails = (TextView) view.findViewById(R.id.jobDetails);
        nameDetails = (TextView) view.findViewById(R.id.nameDetails);

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivityForResult(intent, 10);
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MyProfile.class);
                startActivity(intent);
            }
        });

        tindoPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils utils = new Utils();
                utils.showLayout(getActivity());

//                Utils.toast(getActivity(), getActivity().getResources().getString(R.string.contact_us_details));
            }
        });
        setData(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setData(getActivity());

        if (requestCode == Utils.REQUEST_CODE_SELECT_SOURCE && resultCode == RESULT_OK) {
            String selectedSource = data.getStringExtra(PaymentMethodsActivity.EXTRA_SELECTED_PAYMENT);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(selectedSource);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onActivityResult: " + selectedSource);


            if (jsonObject.length() > 0) {
                cardID = jsonObject.optString("id");
                try {
                    processPayment();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                cardID = "";

            }


        }

    }

    private void processPayment() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("packageType", appSettings.getSelectedRole());
        jsonObject.put("amount", "200");
        jsonObject.put("cardId", cardID);
        Log.d(TAG, "processPayment: " + jsonObject);
        ApiCall.PostMethod(getActivity(), UrlHelper.MAKE_PAYMENT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                Utils utils = new Utils();
                utils.dismissGoldLayout();
                Utils.toast(getActivity(), getResources().getString(R.string.payment_successs));
                appSettings.setUserType(appSettings.getSelectedRole());
            }
        });
    }

}
