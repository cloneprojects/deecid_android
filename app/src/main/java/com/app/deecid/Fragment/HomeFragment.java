package com.app.deecid.Fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.deecid.Activity.ChatActivity;
import com.app.deecid.Activity.ProfileActivity;
import com.app.deecid.Adapters.SwipeCardAdapter;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.Helper.Utils;
import com.app.deecid.R;
import com.app.deecid.SwipeLayout.BinderSwipeStack;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.VolleyCallback;
import com.skyfishjy.library.RippleBackground;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.view.PaymentMethodsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements BinderSwipeStack.SwipeStackListener, View.OnClickListener {
    private static BinderSwipeStack swipeStack;
    private static String TAG = HomeFragment.class.getSimpleName();
    public TextView homeText;
    SwipeCardAdapter swipeCardAdapter;
    JSONArray peopleArray = new JSONArray();
    boolean addStatus;
    int likecountRemaining = 0, superLikeCountRemaining = 0;
    AppSettings appSettings;
    Utils utils;
    Context context;
    RelativeLayout defLayout;
    RelativeLayout rippleLayout;
    RippleBackground rippleEffect;
    RelativeLayout rewindButton, nopeButton, superlikeButton, likeButton, boostButton;
    int currentCount = 0;
    int totalCount;
    CircleImageView userImage;
    View lastView;
    int oldPositionl = 0;
    ImageView rewindIcon, superLikeIcon;
    private int mScreenWidth;
    private FragmentManager fragmentManager;
    private String cardID = "";


    public HomeFragment() {
        // Required empty public constructor
    }

    public static void setViewVisibility(RelativeLayout imageView, float dragDistanceX) {
        if (dragDistanceX > 80 && dragDistanceX < 110) {
            imageView.setAlpha((float) 0.7);
        } else if (dragDistanceX > 110 && dragDistanceX < 130) {
            imageView.setAlpha((float) 0.8);

        } else if (dragDistanceX > 130 && dragDistanceX < 150) {
            imageView.setAlpha((float) 0.9);


        } else if (dragDistanceX > 150) {
            imageView.setAlpha(1);
        }

    }

    public static void resetNext(View mObservedView) {
        View v = mObservedView;

        RelativeLayout like = (RelativeLayout) v.findViewById(R.id.likebanner);
        RelativeLayout superlike = (RelativeLayout) v.findViewById(R.id.superlikebanner);
        RelativeLayout nope = (RelativeLayout) v.findViewById(R.id.nopebanner);
        like.setVisibility(View.INVISIBLE);
        superlike.setVisibility(View.INVISIBLE);
        nope.setVisibility(View.INVISIBLE);
    }

    public void noAds(View mObservedView) {

        RelativeLayout like = (RelativeLayout) mObservedView.findViewById(R.id.likebanner);
        RelativeLayout superlike = (RelativeLayout) mObservedView.findViewById(R.id.superlikebanner);
        RelativeLayout nope = (RelativeLayout) mObservedView.findViewById(R.id.nopebanner);
        like.setVisibility(View.INVISIBLE);
        superlike.setVisibility(View.INVISIBLE);
        nope.setVisibility(View.INVISIBLE);
    }

    public static void update(int which, float dragDistanceX, float dragDistanceY, View mObservedView) {
        View v = mObservedView;
//        Log.d(TAG, "update: " + dragDistanceX);
        if (dragDistanceX < 0) {
            dragDistanceX = dragDistanceX * -1;
        }


        RelativeLayout like = (RelativeLayout) v.findViewById(R.id.likebanner);
        RelativeLayout superlike = (RelativeLayout) v.findViewById(R.id.superlikebanner);
        RelativeLayout nope = (RelativeLayout) v.findViewById(R.id.nopebanner);

        switch (which) {
            case 0:
                like.setVisibility(View.VISIBLE);
                nope.setVisibility(View.INVISIBLE);
                superlike.setVisibility(View.INVISIBLE);
                setViewVisibility(like, dragDistanceX);
                break;
            case 1:
                nope.setVisibility(View.VISIBLE);
                like.setVisibility(View.INVISIBLE);
                superlike.setVisibility(View.INVISIBLE);

                setViewVisibility(nope, dragDistanceX);

                break;
            case 2:
                superlike.setVisibility(View.VISIBLE);
                like.setVisibility(View.INVISIBLE);
                nope.setVisibility(View.INVISIBLE);
                setSuperViewVisibility(superlike, dragDistanceY);

                break;
            default:
                like.setVisibility(View.INVISIBLE);
                superlike.setVisibility(View.INVISIBLE);
                nope.setVisibility(View.INVISIBLE);

        }


    }

    private static void setSuperViewVisibility(RelativeLayout imageView, float dragDistanceX) {
        dragDistanceX = dragDistanceX * -1;
        if (dragDistanceX > 80 && dragDistanceX < 110) {
            imageView.setAlpha((float) 0.7);
        } else if (dragDistanceX > 110 && dragDistanceX < 130) {
            imageView.setAlpha((float) 0.8);

        } else if (dragDistanceX > 130 && dragDistanceX < 150) {
            imageView.setAlpha((float) 0.9);


        } else if (dragDistanceX > 150) {
            imageView.setAlpha(1);
        }
    }

//    private void initValues() throws JSONException {
//
//
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("name", "Abdul Kalam ");
//        jsonObject.put("age", "83");
//        jsonObject.put("Comments", "Youngster Hero");
//        JSONArray imageArray = new JSONArray();
//        JSONObject imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://www.litmusbranding.com/blog/wp-content/uploads/2015/08/abdul-kalam.jpg");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://convergenceservices.in/images/APJ_Abdul_Kalam.jpg");
//        imageArray.put(imgjsonObject);
//        jsonObject.put("images", imageArray);
//        peopleArray.put(jsonObject);
//
//
//        jsonObject = new JSONObject();
//        jsonObject.put("name", "Nivetha ");
//        jsonObject.put("age", "18");
//        jsonObject.put("Comments", "Lorem Ipusm dolar");
//        imageArray = new JSONArray();
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://dhoom.filmipop.com/media//cast/2017/Mar/1488368849-Nivetha-ThomasP.jpg");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://moviegalleri.net/wp-content/gallery/nivetha-thomas-latest-photos/actress_niveda_nivetha_thomas_latest_photos_303085b.jpg");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "http://www.behindwoods.com/tamil-actress/nivedha-thomas/nivedha-thomas-stills-photos-pictures-15.jpg");
//        imageArray.put(imgjsonObject);
//        jsonObject.put("images", imageArray);
//
//        peopleArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("name", "Vijay");
//        jsonObject.put("age", "25");
//        jsonObject.put("Comments", "Lorem Ipusm dolar");
//
//        imageArray = new JSONArray();
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://timesofindia.indiatimes.com/photo/msid-60000731/60000731.jpg?49063");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://img.etimg.com/thumb/msid-61176730,width-300,imgsize-217069,resizemode-4/tax-shadow-over-mersal-star-vijay-i-t-panel-may-take-call.jpg");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "http://www.thenewsminute.com/sites/default/files/styles/news_detail/public/mersal-compressed.jpg?itok=3bQ5wioT");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "http://img.iflicks.in/Articles/2017/Aug/Spotlight/Vijay-Takes-The-Mersal-Route-Again_SECVPF.gif");
//        imageArray.put(imgjsonObject);
//        jsonObject.put("images", imageArray);
//        peopleArray.put(jsonObject);
//
//        jsonObject = new JSONObject();
//        jsonObject.put("name", "Abdul Kalam ");
//        jsonObject.put("age", "83");
//        jsonObject.put("Comments", "Youngster Hero");
//        imageArray = new JSONArray();
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://www.litmusbranding.com/blog/wp-content/uploads/2015/08/abdul-kalam.jpg");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://convergenceservices.in/images/APJ_Abdul_Kalam.jpg");
//        imageArray.put(imgjsonObject);
//        jsonObject.put("images", imageArray);
//        peopleArray.put(jsonObject);
//
//
//        jsonObject = new JSONObject();
//        jsonObject.put("name", "Nivetha ");
//        jsonObject.put("age", "18");
//        jsonObject.put("Comments", "Lorem Ipusm dolar");
//        imageArray = new JSONArray();
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://dhoom.filmipop.com/media//cast/2017/Mar/1488368849-Nivetha-ThomasP.jpg");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://moviegalleri.net/wp-content/gallery/nivetha-thomas-latest-photos/actress_niveda_nivetha_thomas_latest_photos_303085b.jpg");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "http://www.behindwoods.com/tamil-actress/nivedha-thomas/nivedha-thomas-stills-photos-pictures-15.jpg");
//        imageArray.put(imgjsonObject);
//        jsonObject.put("images", imageArray);
//
//        peopleArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("name", "Vijay");
//        jsonObject.put("age", "25");
//        jsonObject.put("Comments", "Lorem Ipusm dolar");
//
//        imageArray = new JSONArray();
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://timesofindia.indiatimes.com/photo/msid-60000731/60000731.jpg?49063");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "https://img.etimg.com/thumb/msid-61176730,width-300,imgsize-217069,resizemode-4/tax-shadow-over-mersal-star-vijay-i-t-panel-may-take-call.jpg");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "http://www.thenewsminute.com/sites/default/files/styles/news_detail/public/mersal-compressed.jpg?itok=3bQ5wioT");
//        imageArray.put(imgjsonObject);
//        imgjsonObject = new JSONObject();
//        imgjsonObject.put("image", "http://img.iflicks.in/Articles/2017/Aug/Spotlight/Vijay-Takes-The-Mersal-Route-Again_SECVPF.gif");
//        imageArray.put(imgjsonObject);
//        jsonObject.put("images", imageArray);
//        peopleArray.put(jsonObject);
//
//
//    }

    public static void setVisible(int which, View mObservedView) {

        try {
            View v = mObservedView;


            RelativeLayout like = (RelativeLayout) v.findViewById(R.id.likebanner);
            RelativeLayout superlike = (RelativeLayout) v.findViewById(R.id.superlikebanner);
            RelativeLayout nope = (RelativeLayout) v.findViewById(R.id.nopebanner);

            switch (which) {
                case 0:
                    like.setVisibility(View.VISIBLE);
                    nope.setVisibility(View.INVISIBLE);
                    superlike.setVisibility(View.INVISIBLE);

                    break;
                case 1:
                    nope.setVisibility(View.VISIBLE);
                    like.setVisibility(View.INVISIBLE);
                    superlike.setVisibility(View.INVISIBLE);


                    break;
                case 2:
                    superlike.setVisibility(View.VISIBLE);
                    like.setVisibility(View.INVISIBLE);
                    nope.setVisibility(View.INVISIBLE);


                    break;
                default:
                    like.setVisibility(View.INVISIBLE);
                    superlike.setVisibility(View.INVISIBLE);
                    nope.setVisibility(View.INVISIBLE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showGoldFromSwipe(Activity context) {
        Utils utils = new Utils();
        utils.showgoldLayout(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();
        PaymentConfiguration.init(context.getResources().getString(R.string.stripe_key));
        utils = new Utils();
        appSettings = new AppSettings(context);
        initViews(view);
        initAdapters();
        try {

            getValuesFromServer();
//            initValues();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListners();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Utils.hideKeyboard(getActivity());

    }

    public void getValuesFromServer() throws JSONException {
        setLoadLay("no");
        rippleEffect.startRippleAnimation();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());

        ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.MAIN_SCREEN, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {


                appSettings.setLikesRemaing(response.optString("Likecount"));
                appSettings.setSuperlikesRemaing(response.optString("Superlikecount"));
                appSettings.setUserType(response.optString("subscription"));

                Log.d(TAG, "usertype: " + response);

                Log.e("swipecount", "Likecount: " + response.optString("Likecount"));
                Log.e("swipecount", "Superlikecount: " + response.optString("Superlikecount"));

                rippleEffect.stopRippleAnimation();

                if (response.has("profile")) {
                    peopleArray = response.optJSONArray("profile");
                    if (peopleArray.length() > 0) {
                        setDefaultLay();

                        swipeCardAdapter = new SwipeCardAdapter(peopleArray, context, fragmentManager, mScreenWidth);


                        try {
                            swipeStack.resetStack();
                        } catch (Exception e) {

                        }

                        swipeStack.setAdapter(swipeCardAdapter);


                        if (!appSettings.getUserType().equalsIgnoreCase("basic")) {
                            if (lastView == null) {
                                disableRewindButton();
                            } else {
                                enableRewindButton();
                            }
                        } else {
                            enableRewindButton();
                        }
                    } else {
                        setLoadLay("yes");
                    }
                } else {
                    setLoadLay("yes");
                }


            }
        });

    }

    private void initListners() {
        rewindButton.setOnClickListener(this);
        nopeButton.setOnClickListener(this);
        superlikeButton.setOnClickListener(this);
        likeButton.setOnClickListener(this);
        boostButton.setOnClickListener(this);
    }

    private void initAdapters() {
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        int mScreenHeight = localDisplayMetrics.heightPixels;
        mScreenWidth = localDisplayMetrics.widthPixels;
        fragmentManager = getActivity().getSupportFragmentManager();

    }

    private void initViews(View view) {
        swipeStack = (BinderSwipeStack) view.findViewById(R.id.swipeStack);
        rewindButton = (RelativeLayout) view.findViewById(R.id.rewindButton);
        nopeButton = (RelativeLayout) view.findViewById(R.id.nopeButton);
        likeButton = (RelativeLayout) view.findViewById(R.id.likeButton);
        superlikeButton = (RelativeLayout) view.findViewById(R.id.superlikeButton);
        boostButton = (RelativeLayout) view.findViewById(R.id.boostButton);
        defLayout = (RelativeLayout) view.findViewById(R.id.defLayout);
        rippleLayout = (RelativeLayout) view.findViewById(R.id.rippleLayout);
        rippleEffect = (RippleBackground) view.findViewById(R.id.rippleEffect);
        userImage = (CircleImageView) view.findViewById(R.id.userImage);
        homeText = (TextView) view.findViewById(R.id.homeText);
        rewindIcon = (ImageView) view.findViewById(R.id.rewindIcon);
        superLikeIcon = (ImageView) view.findViewById(R.id.superLikeIcon);

        Glide.with(getActivity()).load(appSettings.getUserImageUrl()).into(userImage);
        swipeStack.setListener(this);


    }

    private void setDefaultLay() {
        defLayout.setVisibility(View.VISIBLE);
        rippleLayout.setVisibility(View.GONE);
    }

    private void setLoadLay(String no) {
        defLayout.setVisibility(View.GONE);
        rippleLayout.setVisibility(View.VISIBLE);
        if (no.equalsIgnoreCase("yes")) {
            homeText.setText(getResources().getString(R.string.no_one_found_near));
            rippleEffect.stopRippleAnimation();

        } else {
            rippleEffect.startRippleAnimation();

            homeText.setText(getResources().getString(R.string.no_one_found));

        }
    }

    @Override
    public void onViewSwipedToLeft(int position, View mObservedView) {


        lastView = mObservedView;
        try {

            JSONObject jsonObject = peopleArray.optJSONObject(position);

            addStatus = Boolean.parseBoolean(jsonObject.optString("addStatus"));
            Log.e(TAG, "onViewSwipedToLeft: " + addStatus);
            if (!addStatus) {
                updateSwipe(jsonObject.optString("id"), "nope");

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onViewSwipedToRight(int position, View mObservedView) {
        lastView = mObservedView;
//        if (likecountRemaining!=0) {
        try {
            JSONObject jsonObject = peopleArray.optJSONObject(position);
            addStatus = Boolean.parseBoolean(jsonObject.optString("addStatus"));
            Log.e(TAG, "onViewSwipedToLeft: " + addStatus);
            if (!addStatus) {
                updateSwipe(jsonObject.optString("id"), "like");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        } else {
//            rewindCards();
//        }

    }

    @Override
    public void onViewSwipedToTop(int position, View mObservedView) {
        lastView = mObservedView;

//        if (superLikeCountRemaining!=0) {


        try {

            JSONObject jsonObject = peopleArray.optJSONObject(position);

            addStatus = Boolean.parseBoolean(jsonObject.optString("addStatus"));
            // Log.e(TAG, "onViewSwipedToLeft: " + addStatus);
            if (!addStatus) {
                updateSwipe(jsonObject.optString("id"), "superlike");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
//        } else {
//            rewindCards();
//        }


    }

    @Override
    public void onTopCardClicked(int positon, String positio, View mObservedView) {

        if (oldPositionl != positon) {
            oldPositionl = positon;
            currentCount = 0;
        }
        View v = mObservedView;


        ImageView viewPager = (ImageView) v.findViewById(R.id.swipeCardUserImage);
        ViewPager viewPage = (ViewPager) v.findViewById(R.id.viewPage);

        JSONObject jsonObject = new JSONObject();
        jsonObject = peopleArray.optJSONObject(positon);
        JSONArray imgAr = jsonObject.optJSONArray("profile_images");
        totalCount = imgAr.length();

        if (positio.equalsIgnoreCase("left")) {
//            Toast.makeText(context, "left Click", Toast.LENGTH_SHORT).show();
            if (currentCount > 0) {
                currentCount = currentCount - 1;
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1 = imgAr.optJSONObject(currentCount);
                String img = jsonObject1.optString("imageURL");
                Glide.with(getActivity()).load(img).into(viewPager);
                viewPage.setCurrentItem(currentCount);
            }
        } else if (positio.equalsIgnoreCase("click")) {
//            Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show();

            jsonObject = peopleArray.optJSONObject(positon);
            Intent intent = new Intent(getActivity(), ProfileActivity.class);
            intent.putExtra("shouldShow", "yes");

            intent.putExtra("profileDetails", jsonObject.toString());
            startActivityForResult(intent, 121);

        } else {
            int check = currentCount + 1;
            if (check < totalCount) {
                currentCount = currentCount + 1;
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1 = imgAr.optJSONObject(currentCount);
                String img = jsonObject1.optString("imageURL");
                Glide.with(getActivity()).load(img).into(viewPager);
                viewPage.setCurrentItem(currentCount);
            }
//            Toast.makeText(context, "right Click", Toast.LENGTH_SHORT).show();


        }

    }

    public void updateSwipe(String receiverId, final String swipeType) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("senderID", appSettings.getUserId());
        jsonObject.put("receiverID", receiverId);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("swipeType", swipeType);
        ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.SWIPE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSwipeSuccess: " + response);
                if (!swipeType.equalsIgnoreCase("nope")) {
                    appSettings.reduceSwipeCount(swipeType);
                }

//                if (response.optString("isEnabled").equalsIgnoreCase("true")) {

                if (response.has("ismatch")) {
                    if (response.optString("ismatch").equalsIgnoreCase("true")) {
                        showMatchedDialog(response);
                    }
                }

                if (!appSettings.getUserType().equalsIgnoreCase("basic")) {
                    if (lastView == null) {
                        disableRewindButton();
                    } else {
                        enableRewindButton();
                    }
                } else {
                    enableRewindButton();
                }
//                } else {
//                    utils.showgoldLayout(context);
//                    rewindCards();
//                }
            }


        });

    }

    private void enableRewindButton() {
        rewindIcon.setImageResource(R.drawable.rewind_icon);
        rewindButton.setClickable(true);

    }

    private void disableRewindButton() {
        rewindIcon.setImageResource(R.drawable.empty_rewind);
        rewindButton.setClickable(false);
    }

    private void enableSuperLikeButton() {
        superLikeIcon.setImageResource(R.drawable.super_like_icon);
        superlikeButton.setClickable(true);

    }

    private void disableSuperLikeButton() {
        superLikeIcon.setImageResource(R.drawable.empty_super_like);
        superlikeButton.setClickable(false);
    }

    private void showMatchedDialog(final JSONObject jsonObject) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.match_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        CircleImageView firstUserImage = (CircleImageView) dialog.findViewById(R.id.firstUserImage);
        CircleImageView secondUserImage = (CircleImageView) dialog.findViewById(R.id.secondUserImage);
        LinearLayout superLikedLayout = (LinearLayout) dialog.findViewById(R.id.superLikedLayout);
        Button confirmButton = (Button) dialog.findViewById(R.id.confirmButton);
        TextView continues = (TextView) dialog.findViewById(R.id.continues);
        Glide.with(getActivity()).load(appSettings.getUserImageUrl()).into(firstUserImage);
        if (jsonObject.optString("isSuperLiked").equalsIgnoreCase("1")) {
            superLikedLayout.setVisibility(View.VISIBLE);
        } else {
            superLikedLayout.setVisibility(View.GONE);
        }
        Glide.with(getActivity()).load(jsonObject.optString("receiverImage")).into(secondUserImage);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject jsonObject1 = new JSONObject();
                try {
                    jsonObject1.put("displayPic", jsonObject.optString("receiverImage"));
                    jsonObject1.put("userName", jsonObject.optString("receiverName"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("values", jsonObject1.toString());
                intent.putExtra("type", "click");

                intent.putExtra("receiverId", jsonObject.optString("receiverID"));
                context.startActivity(intent);

               /* try {
                    MainActivity.viewPager.setCurrentItem(1);
                }
                catch (Exception e)
                {

                }*/

                dialog.dismiss();

            }
        });

        continues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onStackEmpty() {

        lastView = null;

        try {
            getValuesFromServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {

        if (view == boostButton) {
            utils.showgoldLayout(getActivity());

        } else if (view == likeButton) {

            if (appSettings.getUserType().equalsIgnoreCase("basic")) {
                if (Integer.parseInt(appSettings.getLikesRemaing()) != 0) {
                    lastView = swipeStack.getTopView();
                    swipeStack.swipeTopViewToRight();

                } else {
                    utils.showgoldLayout(getActivity());
                }
            } else {
                lastView = swipeStack.getTopView();
                swipeStack.swipeTopViewToRight();


            }


        } else if (view == nopeButton) {
            if (swipeStack.getChildCount() != 0) {
                lastView = swipeStack.getTopView();
                swipeStack.swipeTopViewToLeft();
            }

        } else if (view == superlikeButton) {
            if (Integer.parseInt(appSettings.getSuperlikesRemaing()) != 0) {
                lastView = swipeStack.getTopView();
                swipeStack.swipeTopViewToTop();
            } else {
                utils.showgoldLayout(getActivity());
            }

        } else if (view == rewindButton) {
            rewindCards();
        }
    }

    private void rewindCards() {
        if (!appSettings.getUserType().equalsIgnoreCase("basic")) {
            if (lastView != null)
                if (swipeStack.getChildCount() != 0) {
                    swipeStack.resetLast(lastView, swipeStack.getChildCount());
                } else {
                    swipeStack.resetLast(lastView, 0);

                }
            lastView = null;
            disableRewindButton();
        } else {
            utils.showgoldLayout(getActivity());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 121) {
            if (resultCode == RESULT_OK) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (data.getStringExtra("swipe").equalsIgnoreCase("like")) {
                            lastView = swipeStack.getTopView();
                            swipeStack.swipeTopViewToRight();

                        } else if (data.getStringExtra("swipe").equalsIgnoreCase("nope")) {
                            lastView = swipeStack.getTopView();
                            swipeStack.swipeTopViewToLeft();

                        } else if (data.getStringExtra("swipe").equalsIgnoreCase("superlike")) {
                            lastView = swipeStack.getTopView();
                            swipeStack.swipeTopViewToTop();

                        }
                    }
                }, 500);

            }
        }


        if (requestCode == Utils.REQUEST_CODE_SELECT_SOURCE && resultCode == RESULT_OK) {
            String selectedSource = data.getStringExtra(PaymentMethodsActivity.EXTRA_SELECTED_PAYMENT);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(selectedSource);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onActivityResult: " + selectedSource);


            if (jsonObject.length() > 0) {
                cardID = jsonObject.optString("id");
                try {
                    processPayment();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                cardID = "";

            }


        }

    }

    private void processPayment() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("packageType", appSettings.getSelectedRole());
        jsonObject.put("amount", "200");
        jsonObject.put("cardId", cardID);
        Log.d(TAG, "processPayment: " + jsonObject);
        ApiCall.PostMethod(getActivity(), UrlHelper.MAKE_PAYMENT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                utils.dismissGoldLayout();
                Utils.toast(getActivity(), getResources().getString(R.string.payment_successs));

                appSettings.setUserType(appSettings.getSelectedRole());
            }
        });
    }
}
