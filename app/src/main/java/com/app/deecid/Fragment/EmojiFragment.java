package com.app.deecid.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.app.deecid.Activity.ChatActivity;
import com.app.deecid.R;
import com.app.deecid.Services.EmojiMessageEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;


public class EmojiFragment extends Fragment {
    String type;
    private int CLICK_ACTION_THRESHOLD = 200;
    private float startX;
    private float startY;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_emoji, container, false);

        ImageView backgroundImage =  v.findViewById(R.id.emoji);
        ImageView backgroundValue =  v.findViewById(R.id.backgroundValue);

        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(backgroundImage);

//        pageText.setText(getArguments().getString("title"));

        final String positon=getArguments().getString("position");
        if (positon.equalsIgnoreCase("0"))
        {
            Glide.with(getActivity()).load(R.drawable.heart_emoji_trans).into(imageViewTarget);
            Glide.with(getActivity()).load(R.drawable.bg_heart).into(backgroundValue);
            type="heart";
        } else if (positon.equalsIgnoreCase("1"))
        {
            Glide.with(getActivity()).load(R.drawable.emoji_laugh).into(imageViewTarget);
            Glide.with(getActivity()).load(R.drawable.bg_laugh).into(backgroundValue);
            type="laugh";
        } else if (positon.equalsIgnoreCase("2"))
        {
            Glide.with(getActivity()).load(R.drawable.wow_emoji).into(imageViewTarget);
            Glide.with(getActivity()).load(R.drawable.bg_wow).into(backgroundValue);
            type="wow";
        } else
        {
            Glide.with(getActivity()).load(R.drawable.angry_emoji).into(imageViewTarget);
            Glide.with(getActivity()).load(R.drawable.bg_angry).into(backgroundValue);
            type="angry";
        }

//        backgroundImage



        backgroundImage. setOnTouchListener(new View.OnTouchListener() {
            private static final int MAX_CLICK_DURATION = 200;
            private long startClickTime;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        startX = event.getX();
                        startY = event.getY();

                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;

                            //click event has occurred

                            float endX = event.getX();
                            float endY = event.getY();
                            if (isAClick(startX, endX, startY, endY)) {
                                if (ChatActivity.isExpanded) {
                                    EventBus.getDefault().post(new EmojiMessageEvent(type));
                                    ChatActivity.startDropping(positon);
                                }
                            }




                    }
                }
                return true;
            }
        });





//        backgroundImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

        return v;
    }


    private boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
    }

    public static EmojiFragment newInstance(String background) {

        EmojiFragment f = new EmojiFragment();
        Bundle b = new Bundle();

        b.putString("position", background);

        f.setArguments(b);

        return f;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser)
        {

        }
        else{

        }
    }

}
