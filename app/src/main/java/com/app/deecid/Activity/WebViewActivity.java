package com.app.deecid.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.deecid.Helper.Utils;
import com.app.deecid.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WebViewActivity extends AppCompatActivity {
    WebView webView;
    TextView toolbarTitle, contentValue;
    String url;
    ImageView backIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        initViews();
//        loadWebView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadWebView() {

        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }

    private void initViews() {
//        webView = (WebView) findViewById(R.id.webView);
        backIcon = (ImageView) findViewById(R.id.backIcon);
        toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        contentValue = (TextView) findViewById(R.id.contentValue);
        toolbarTitle.setText(getIntent().getStringExtra("title"));
        url = getIntent().getStringExtra("url");

        contentValue.setText(url);

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            Utils.show(WebViewActivity.this);

            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Utils.show(WebViewActivity.this);
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            System.out.println("on finish");
            Utils.dismiss(WebViewActivity.this);

        }
    }

}
