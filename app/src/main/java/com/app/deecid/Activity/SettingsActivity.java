package com.app.deecid.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.app.deecid.Adapters.LocationListAdapter;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.Helper.Utils;
import com.app.deecid.R;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.VolleyCallback;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.view.PaymentMethodsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingsActivity extends AppCompatActivity {
    public static ImageView backIcon, markerLocation;
    public static String selectedLocationId;
    public static ImageView isSelected;
    public static boolean isMyLocationSelected;
    public boolean isLocationVisible = false;
    LinearLayout myLocationLay, locationLists;
    CrystalRangeSeekbar ageRange;
    CrystalSeekbar distanceRange;
    TextView genderText, ageText, distanceText, maleSwitchTv, femaleSwitchTv;
    Switch maleSwitch, femaleSwitch;
    CardView tinderGold, tinderPlus;
    TextView button2_title, button1_title;
    Utils utils;
    String button1_content = "", button2_content = "";
    LinearLayout myCurrentLocation;
    int fromAge, toAge;
    LinearLayout logOut, deleteAccount;
    String genderTextVale = "";
    private String sugarPref, sugarValue;


    RecyclerView locationListsRecyclerView;
    AppSettings appSettings = new AppSettings(SettingsActivity.this);
    Switch showMeTinder;
    LinearLayout helpSupport, privacyPolicy;
    CardView getSuperLike, getBoosts;
    TextView addNewLocation;
    private String TAG = SettingsActivity.class.getSimpleName();
    private JSONArray locationArray = new JSONArray();
    private LocationListAdapter locationListAdapter;
    private String cardID = "";

    public static void clearMyLocation() {
        isMyLocationSelected = false;

        markerLocation.setImageResource(R.drawable.unselected_marker);
        isSelected.setVisibility(View.INVISIBLE);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        PaymentConfiguration.init(getResources().getString(R.string.stripe_key));

        initViews();
        initListners();


    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            getSettingsValue();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getSettingsValue() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethod(SettingsActivity.this, UrlHelper.GET_SETTINGS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                JSONArray jsonArray = response.optJSONArray("results");
                JSONObject object = jsonArray.optJSONObject(0);
                appSettings.setGenderPref(object.optString("genderPreference"));
                handleGenderPreference(object.optString("genderPreference"));
                handleDistanceValue(object.optString("maxDistance"));
                handleAgeValue(object.optString("toAgePreference"), object.optString("fromAgePreference"));
                if (object.optString("showMeTinder").equalsIgnoreCase("1")) {
                    showMeTinder.setChecked(true);
                } else {
                    showMeTinder.setChecked(false);
                }
                if (object.optString("isCurrentLocationSelected").equalsIgnoreCase("1")) {
                    selectMyLocation();
                } else {
                    clearMyLocation();
                }
                initAdapters(response);

                button1_title.setText(object.optString("button1_title"));
                button2_title.setText(object.optString("button2_title"));

                button1_content = object.optString("button1_content");
                button2_content = object.optString("button2_content");
            }
        });
    }

    private void initAdapters(JSONObject response) {
        locationArray = response.optJSONArray("address");

        if (locationArray.length() > 0) {
            locationListsRecyclerView.setVisibility(View.VISIBLE);
            locationListAdapter = new LocationListAdapter(locationArray, SettingsActivity.this);
            LinearLayoutManager linearLayoutManagers = new LinearLayoutManager(SettingsActivity.this, LinearLayoutManager.VERTICAL, false);
            locationListsRecyclerView.setLayoutManager(linearLayoutManagers);
            locationListsRecyclerView.setAdapter(locationListAdapter);
        } else {
            locationListsRecyclerView.setVisibility(View.GONE);
        }


    }

    private void handleAgeValue(String toAgePreference, String fromAgePreference) {
        int maxValue = Integer.parseInt(toAgePreference);
        int minValue = Integer.parseInt(fromAgePreference);
        fromAge = minValue;
        toAge = maxValue;
        if (maxValue == 55) {
            ageText.setText("" + minValue + " - " + maxValue + "+");

        } else {
            ageText.setText("" + minValue + " - " + maxValue);

        }
        ageRange.setDataType(CrystalSeekbar.DataType.INTEGER).setMinValue(18).setMaxValue(55).setMinStartValue(minValue).setMaxStartValue(maxValue).apply();


    }

    private void handleDistanceValue(String maxDistance) {
        int minValue = Integer.parseInt(maxDistance);
        distanceText.setText(maxDistance);
        distanceRange.setMinStartValue(minValue).setMinValue(2).setMaxValue(500).apply();


    }

    private void handleGenderPreference(String genderPreference) {
        if (genderPreference.equalsIgnoreCase("sugar_baby_boy")) {

            maleSwitchTv.setText(R.string.sugar_baby_boy);
            femaleSwitchTv.setText(R.string.sugar_baby_girl);

            maleSwitch.setChecked(true);
            femaleSwitch.setChecked(false);

            // genderText.setText(getString(R.string.male_text));


        } else if (genderPreference.equalsIgnoreCase("sugar_daddy")) {

            maleSwitchTv.setText(R.string.sugar_daddy);
            femaleSwitchTv.setText(R.string.sugar_mommy);

            maleSwitch.setChecked(true);
            femaleSwitch.setChecked(false);

            // genderText.setText(getString(R.string.female_text));

        } else if (genderPreference.equalsIgnoreCase("sugar_baby_girl")) {
            maleSwitchTv.setText(R.string.sugar_baby_boy);
            femaleSwitchTv.setText(R.string.sugar_baby_girl);

            maleSwitch.setChecked(false);
            femaleSwitch.setChecked(true);

            // genderText.setText(getString(R.string.male_female_text));

        } else if (genderPreference.equalsIgnoreCase("sugar_mummy")) {

            maleSwitchTv.setText(R.string.sugar_daddy);
            femaleSwitchTv.setText(R.string.sugar_mommy);

            maleSwitch.setChecked(false);
            femaleSwitch.setChecked(true);

            //  genderText.setText(getString(R.string.male_female_text));
        }
    }

    private void initViews() {
        myLocationLay = (LinearLayout) findViewById(R.id.myLocationLay);
        locationLists = (LinearLayout) findViewById(R.id.locationLists);
        logOut = (LinearLayout) findViewById(R.id.logOut);
        deleteAccount = (LinearLayout) findViewById(R.id.deleteAccount);
        ageRange = (CrystalRangeSeekbar) findViewById(R.id.ageRange);
        distanceRange = (CrystalSeekbar) findViewById(R.id.distanceRange);
        genderText = (TextView) findViewById(R.id.genderText);
        addNewLocation = (TextView) findViewById(R.id.addNewLocation);
        maleSwitch = (Switch) findViewById(R.id.maleSwitch);
        femaleSwitch = (Switch) findViewById(R.id.femaleSwitch);
        showMeTinder = (Switch) findViewById(R.id.showMeTinder);
        ageText = (TextView) findViewById(R.id.ageText);
        distanceText = (TextView) findViewById(R.id.distanceText);
        maleSwitchTv = (TextView) findViewById(R.id.maleSwitchTv);
        femaleSwitchTv = (TextView) findViewById(R.id.femaleSwitchTv);
        backIcon = (ImageView) findViewById(R.id.backIcon);
        markerLocation = (ImageView) findViewById(R.id.markerLocation);
        isSelected = (ImageView) findViewById(R.id.isSelected);
        tinderGold = (CardView) findViewById(R.id.tinderGold);
        tinderPlus = (CardView) findViewById(R.id.tinderPlus);
        getSuperLike = (CardView) findViewById(R.id.getSuperLike);
        getBoosts = (CardView) findViewById(R.id.getBoosts);
        helpSupport = (LinearLayout) findViewById(R.id.helpSupport);
        privacyPolicy = (LinearLayout) findViewById(R.id.privacyPolicy);
        myCurrentLocation = (LinearLayout) findViewById(R.id.myCurrentLocation);
        locationListsRecyclerView = (RecyclerView) findViewById(R.id.locationListsRecyclerView);
        button2_title = (TextView) findViewById(R.id.button2_title);
        button1_title = (TextView) findViewById(R.id.button1_title);

        Log.e(TAG, "usertype: " + appSettings.getUserType());
        if (appSettings.getUserType().equalsIgnoreCase("basic")) {

            tinderGold.setVisibility(View.VISIBLE);
            tinderPlus.setVisibility(View.VISIBLE);
            getSuperLike.setVisibility(View.VISIBLE);
            getBoosts.setVisibility(View.VISIBLE);

        } else if (appSettings.getUserType().equalsIgnoreCase("plus")) {

            tinderGold.setVisibility(View.VISIBLE);
            tinderPlus.setVisibility(View.GONE);
            getSuperLike.setVisibility(View.GONE);
            getBoosts.setVisibility(View.GONE);

        } else if (appSettings.getUserType().equalsIgnoreCase("gold")) {

            tinderGold.setVisibility(View.GONE);
            tinderPlus.setVisibility(View.GONE);
            getSuperLike.setVisibility(View.GONE);
            getBoosts.setVisibility(View.GONE);
        }

        utils = new Utils();

    }

    public void selectMyLocation() {
        isMyLocationSelected = true;
        selectedLocationId = "";
        markerLocation.setImageResource(R.drawable.selected_marker);
        isSelected.setVisibility(View.VISIBLE);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.REQUEST_CODE_SELECT_SOURCE && resultCode == RESULT_OK) {
            String selectedSource = data.getStringExtra(PaymentMethodsActivity.EXTRA_SELECTED_PAYMENT);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(selectedSource);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onActivityResult: " + selectedSource);


            if (jsonObject.length() > 0) {
                cardID = jsonObject.optString("id");
                try {
                    processPayment();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                cardID = "";

            }


        }

    }

    private void processPayment() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("packageType", appSettings.getSelectedRole());
        jsonObject.put("amount", "200");
        jsonObject.put("cardId", cardID);
        Log.d(TAG, "processPayment: " + jsonObject);
        ApiCall.PostMethod(SettingsActivity.this, UrlHelper.MAKE_PAYMENT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                utils.dismissGoldLayout();
                Utils.toast(SettingsActivity.this, getResources().getString(R.string.payment_successs));
                appSettings.setUserType(appSettings.getSelectedRole());
            }
        });
    }

    private void initListners() {

        myLocationLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLocationVisible) {
                    isLocationVisible = false;
                    locationLists.setVisibility(View.GONE);
                } else {
                    isLocationVisible = true;
                    locationLists.setVisibility(View.VISIBLE);
                }
            }
        });

        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
                intent.putExtra("url", button2_content);
                intent.putExtra("title", button2_title.getText().toString());
                startActivity(intent);

            }
        });

        addNewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!appSettings.getUserType().equalsIgnoreCase("basic")) {
                    Intent intent = new Intent(SettingsActivity.this, MapActivity.class);
                    startActivity(intent);
                } else {
                    utils.showgoldLayout(SettingsActivity.this);
                }
            }
        });


        maleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (maleSwitch.isChecked()) {
                    femaleSwitch.setChecked(false);
                    if (maleSwitchTv.getText().toString().equalsIgnoreCase("Sugar Baby boy")) {
                        sugarPref = "sugar_baby_boy";
                    } else if (maleSwitchTv.getText().toString().equalsIgnoreCase("Sugar Daddy")) {
                        sugarPref = "sugar_daddy";
                    }
                } else {
                    femaleSwitch.setChecked(true);
                    if (femaleSwitchTv.getText().toString().equalsIgnoreCase("Sugar Baby girl")) {
                        sugarPref = "sugar_baby_girl";
                    } else if (femaleSwitchTv.getText().toString().equalsIgnoreCase("Sugar Mommy")) {
                        sugarPref = "sugar_mummy";
                    }
                }
                Log.e(TAG, "onCheckedMaleChanged: " + sugarPref);
            }
        });
        femaleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (femaleSwitch.isChecked()) {
                    maleSwitch.setChecked(false);
                    if (femaleSwitchTv.getText().toString().equalsIgnoreCase("Sugar Baby girl")) {
                        sugarPref = "sugar_baby_girl";
                    } else if (femaleSwitchTv.getText().toString().equalsIgnoreCase("Sugar Mommy")) {
                        sugarPref = "sugar_mummy";
                    }
                } else {
                    maleSwitch.setChecked(true);
                    if (maleSwitchTv.getText().toString().equalsIgnoreCase("Sugar Baby boy")) {
                        sugarPref = "sugar_baby_boy";
                    } else if (maleSwitchTv.getText().toString().equalsIgnoreCase("Sugar Daddy")) {
                        sugarPref = "sugar_daddy";
                    }
                }
                Log.e(TAG, "onCheckedFemaleChanged: " + sugarPref);
            }
        });

//        maleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//
//                if (maleSwitch.isChecked()) {
//
//                    if (femaleSwitch.isChecked()) {
//                        genderTextVale = getString(R.string.male_female_text);
//
//                    }
//
//                } else {
//
//                    if (femaleSwitch.isChecked()) {
//                        genderTextVale = getString(R.string.female_text);
//
//                    } else {
//                        genderTextVale = getString(R.string.female_text);
//                        femaleSwitch.setChecked(true);
//                    }
//                }
//
//                genderText.setText(genderTextVale);
//
//            }
//        });
//
//        femaleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                if (femaleSwitch.isChecked()) {
//                    if (maleSwitch.isChecked()) {
//                        genderTextVale = getString(R.string.male_female_text);
//
//
//                    }
//
//                } else {
//
//                    if (maleSwitch.isChecked()) {
//                        genderTextVale = getString(R.string.male_text);
//
//
//                    } else {
//                        genderTextVale = getString(R.string.male_text);
//
//                        maleSwitch.setChecked(true);
//                    }
//                }
//
//                genderText.setText(genderTextVale);
//            }
//        });


        tinderGold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showgoldLayout();
                utils.showgoldLayout(SettingsActivity.this);
                //utils.showGooglePay(SettingsActivity.this);
//                Utils.toast(SettingsActivity.this, getResources().getString(R.string.contact_us_details));

            }
        });

        getBoosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showgoldLayout();
                utils.showgoldLayout(SettingsActivity.this);

//                Utils.toast(SettingsActivity.this, getResources().getString(R.string.contact_us_details));

            }
        });

        getSuperLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showgoldLayout();
                utils.showgoldLayout(SettingsActivity.this);

//                Utils.toast(SettingsActivity.this, getResources().getString(R.string.contact_us_details));

            }
        });

        tinderPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showLayout();
                utils.showLayout(SettingsActivity.this);

//                Utils.toast(SettingsActivity.this, getResources().getString(R.string.contact_us_details));

            }
        });

        ageRange.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                fromAge = minValue.intValue();
                toAge = maxValue.intValue();
                if (maxValue.intValue() == 55) {

                    ageText.setText("" + minValue + " - " + maxValue + "+");

                } else {
                    ageText.setText("" + minValue + " - " + maxValue);

                }
            }
        });


        distanceRange.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                distanceText.setText("" + value);

            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    logOutServer();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        deleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    deleteAccountApi();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    sendValues();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        helpSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
                intent.putExtra("url", button1_content);
                intent.putExtra("title", button1_title.getText().toString());
                startActivity(intent);
            }
        });

        myCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isMyLocationSelected) {
                    selectMyLocation();
                } else {
                    try {
                        locationListAdapter.refreshAdapter();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    selectMyLocation();
                }
            }
        });

    }


    private void logOutServer() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(SettingsActivity.this, UrlHelper.LOG_OUT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                logout();
            }
        });
    }

    private void logout() {
        Intent intent = new Intent(SettingsActivity.this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        appSettings.setIsLogged("false");
        startActivity(intent);
    }

    private void deleteAccountApi() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(SettingsActivity.this, UrlHelper.DELETE_ACCCOUNT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                logout();
            }
        });

    }

    private void sendValues() throws Exception {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("genderPreference", sugarPref);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("maxDistance", distanceText.getText().toString());
        jsonObject.put("fromAgePreference", "" + fromAge);
        jsonObject.put("toAgePreference", "" + toAge);
        if (showMeTinder.isChecked()) {
            jsonObject.put("showMeTinder", "1");

        } else {
            jsonObject.put("showMeTinder", "0");
        }

        if (isMyLocationSelected) {
            jsonObject.put("isCurrentLocationSelected", "1");
            jsonObject.put("addressId", "0");

        } else {
            jsonObject.put("isCurrentLocationSelected", "0");
            jsonObject.put("addressId", selectedLocationId);

        }
//                    jsonObject.put("maxDistance",m);

        ApiCall.PostMethod(SettingsActivity.this, UrlHelper.SETTINGS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            sendValues();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
