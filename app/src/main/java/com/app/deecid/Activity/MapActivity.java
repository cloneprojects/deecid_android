package com.app.deecid.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.app.deecid.Adapters.AutoCompleteAdapter;
import com.app.deecid.Application.AppController;
import com.app.deecid.Autocomplete.PlacePredictions;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.Helper.Utils;
import com.app.deecid.R;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.LocationCallBack;
import com.app.deecid.Volley.VolleyCallback;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.InfoWindowAdapter {
    String dist = "";

    GoogleMap map;
    ImageView backButton;
    SupportMapFragment mapFragment;
    ImageView myLocation;
    TextView cityName, stateName;
    String cityNameValue = "", stateNameValue = "", finalcityName = "";
    String selectedLat, selectedLong;
    EditText searchBox;
    ListView mAutoCompleteList;
    AppSettings appSettings;
    LinearLayout searchLists;
    AutoCompleteAdapter mAutoCompleteAdapter;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private String TAG = MapActivity.class.getSimpleName();
    private int value = 0;
    private PlacePredictions predictions = new PlacePredictions();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        appSettings = new AppSettings(MapActivity.this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        initlocation();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            settingsrequest();
            initlocationRequest();
        }


        initViews();
        initListners();


    }


    private void initListners() {
        mAutoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                // pass the result to the calling activity
                if (mGoogleApiClient != null) {

                    Places.GeoDataApi.getPlaceById(mGoogleApiClient, predictions.getPlaces().get(position).getPlaceID())
                            .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                @Override
                                public void onResult(PlaceBuffer places) {
                                    Log.d("onResult: ", "status:" + places.getStatus());
                                    if (places.getStatus().isSuccess()) {
                                        searchLists.setVisibility(View.GONE);
                                        searchBox.setText("");
                                        Utils.hideKeyboard(MapActivity.this);
                                        mAutoCompleteList.invalidate();
                                        mAutoCompleteAdapter.clear();


                                        Place myPlace = places.get(0);
                                        setMarkerAndZoom(myPlace.getLatLng());

                                    }

                                    places.release();
                                }
                            });
                }
            }
        });

    }


    @Override
    public View getInfoWindow(Marker marker) {
        View view = getLayoutInflater().inflate(R.layout.custom_info_layout, null);
        view.setLayoutParams(new LinearLayoutCompat.LayoutParams(1000, 85));
        cityName = view.findViewById(R.id.cityName);
        stateName = view.findViewById(R.id.stateName);

        cityName.setText(cityNameValue);
        stateName.setText(stateNameValue);
        return view;
    }

    public String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/place/autocomplete/json");
        urlString.append("?input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        urlString.append("&location=");
        urlString.append(selectedLat + "," + selectedLong); // append lat long of current location to show nearby results.
        urlString.append("&radius=500&language=en");
        urlString.append("&key=" + getResources().getString(R.string.map_key));

        Log.d("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }


    private void searchPlaces() {
        JSONObject object = new JSONObject();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getPlaceAutoCompleteUrl(searchBox.getText().toString()), object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("SearchResponse", response.toString());
                if (response.optString("status").equalsIgnoreCase("ok")) {
//                    JSONArray jsonArray = response.optJSONArray("predictions");
//                    JSONArray finalArray=new JSONArray();
//                    for (int i=0;i<jsonArray.length();i++)
//                    {
//                        JSONObject jsonObject=jsonArray.optJSONObject(i);
//                        JSONArray termsArray=jsonObject.optJSONArray("terms");
//
//                        if (termsArray.length()>3)
//                        {
//                            String cityName=termsArray.optJSONObject(1).optString("value");
//                            String  stateName=termsArray.optJSONObject(2).optString("value");
//                            JSONObject jsonObject1=new JSONObject();
//                            try {
//                                jsonObject1.put("city",cityName);
//                                jsonObject1.put("state",stateName);
//                                finalArray.put(jsonObject1);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//
//                    Log.d(TAG, "onResponse: "+finalArray);

                    Gson gson = new Gson();
                    predictions = gson.fromJson(response.toString(), PlacePredictions.class);
                    if (mAutoCompleteAdapter == null) {
                        mAutoCompleteList.setVisibility(View.VISIBLE);
                        mAutoCompleteAdapter = new AutoCompleteAdapter(MapActivity.this, predictions.getPlaces(), MapActivity.this);
                        mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
                    } else {
                        mAutoCompleteList.setVisibility(View.VISIBLE);
                        mAutoCompleteAdapter.clear();
                        mAutoCompleteAdapter.addAll(predictions.getPlaces());
                        mAutoCompleteAdapter.notifyDataSetChanged();
                        mAutoCompleteList.invalidate();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("SearchResponse", error.toString());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = getLayoutInflater().inflate(R.layout.custom_info_layout, null);
        view.setLayoutParams(new LinearLayoutCompat.LayoutParams(520, 230));

        return null;
    }


    private View prepareInfoView(Marker marker) {
        //prepare InfoView programmatically

        View view = getLayoutInflater().inflate(R.layout.custom_info_layout, null);
        view.setLayoutParams(new LinearLayoutCompat.LayoutParams(520, 230));
//        nameView = view.findViewById(R.id.providerName);
//        distance = view.findViewById(R.id.distance);
//        providerRating = view.findViewById(R.id.providerRating);

//        nameView.setText(providers.optJSONObject(selectedposition).optString("name"));
//        distance.setText(disntac);

        return view;
    }


    private void initlocation() {
        checkCallingOrSelfPermission("");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();


    }


    private void initlocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000);

    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MapActivity.this, 5);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    private void initViews() {

        backButton = (ImageView) findViewById(R.id.backButton);
        myLocation = (ImageView) findViewById(R.id.myLocation);
        searchBox = (EditText) findViewById(R.id.searchBox);
        mAutoCompleteList = (ListView) findViewById(R.id.mAutoCompleteList);
        searchLists = (LinearLayout) findViewById(R.id.searchLists);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchLists.setVisibility(View.GONE);
                try {
                    LatLng myLocation = new LatLng(getMyLocation().getLatitude(), getMyLocation().getLongitude());
                    setMarkerAndZoom(myLocation);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        searchBox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                searchLists.setVisibility(View.VISIBLE);
                return false;
            }
        });

        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (searchBox.getText().toString().length() > 3) {

                    searchPlaces();
                } else {
                    try {
                        if (mAutoCompleteAdapter.getCount() > 0) {
                            mAutoCompleteList.invalidate();
                            mAutoCompleteAdapter.clear();
                        }
                    } catch (Exception e) {

                    }

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        searchLists.setVisibility(View.GONE);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        map.setMyLocationEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);

        map.setInfoWindowAdapter(this);
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {


                setMarkerAndZoom(latLng);


            }
        });
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Log.d(TAG, "onInfoWindowClick: " + marker.getPosition().latitude);
                try {
                    addAddress(marker.getPosition().latitude, marker.getPosition().longitude, finalcityName, stateNameValue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void addAddress(double latitude, double longitude, String cityNameValue, String stateNameValue) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("newLattitude", "" + latitude);
        jsonObject.put("newLongitude", "" + longitude);
        jsonObject.put("cityname", cityNameValue);
        jsonObject.put("address", stateNameValue);
        ApiCall.PostMethod(MapActivity.this, UrlHelper.ADD_ADDRESS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);

                finish();

            }
        });
    }


    public void setMarkerAndZoom(LatLng latLng) {
        map.clear();
        cityNameValue = getResources().getString(R.string.searching);
        stateNameValue = "";
        final Marker newMarker = map.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(bitmapDescriptorFromVector(MapActivity.this, R.drawable.ic_marker_blue_dot))
                .snippet(latLng.toString()));
        newMarker.showInfoWindow();


        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(14).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        Utils.getLocationAddres(MapActivity.this, "" + latLng.latitude, "" + latLng.longitude, new LocationCallBack() {
            @Override
            public void onLocationSuccess(String city, String state, String country) {
                Log.d(TAG, "onLocationSuccess: " + city + "/" + state + "/" + country);
                cityNameValue = getResources().getString(R.string.go_to) + " " + city;
                finalcityName = city;
                stateNameValue = state;

                if (finalcityName.trim().length() == 0) {
                    finalcityName = stateNameValue;
                }
                newMarker.showInfoWindow();
                newMarker.setTitle(newMarker.getId());

            }
        });


    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressLint("MissingPermission")
    private Location getMyLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        @SuppressLint("MissingPermission") Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (myLocation == null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            String provider = lm.getBestProvider(criteria, true);
            myLocation = lm.getLastKnownLocation(provider);
        }
        return myLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        CameraUpdate center =
                CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
                        location.getLongitude()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        map.moveCamera(center);
        map.animateCamera(zoom);

        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {

                initlocationRequest();
            }
        }

        if (requestCode == 1) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initlocationRequest();
                settingsrequest();

            }

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {

            }
        } else {

            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            Utils.log(TAG, "mylat:" + currentLatitude + ",mylong:" + currentLongitude);
            if (currentLatitude != 0.0 && currentLongitude != 0.0) {

//                    setlocation();

            }

        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }

}
