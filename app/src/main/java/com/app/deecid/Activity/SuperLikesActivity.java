package com.app.deecid.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.app.deecid.Adapters.SuperLikesAdapter;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.R;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SuperLikesActivity extends AppCompatActivity {
    private static final String TAG = SuperLikesActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private AppSettings appSettings;
    private SuperLikesAdapter superLikesAdapter;
    private JSONArray mJsonArray;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_likes);
        appSettings = new AppSettings(SuperLikesActivity.this);
        recyclerView = findViewById(R.id.superLikesRv);
        ImageView backIcon = findViewById(R.id.backIcon);
        loadList();

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void loadList() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("accessToken", appSettings.getAccessToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethod(SuperLikesActivity.this, UrlHelper.SEE_LIKED_LIST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                mJsonArray = response.optJSONArray("LikedPerson");
                if (mJsonArray.length() == 0) {
                    recyclerView.setVisibility(View.GONE);

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    superLikesAdapter = new SuperLikesAdapter(SuperLikesActivity.this, mJsonArray);
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(SuperLikesActivity.this, 2);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    recyclerView.setAdapter(superLikesAdapter);
                }
            }
        });
    }
}