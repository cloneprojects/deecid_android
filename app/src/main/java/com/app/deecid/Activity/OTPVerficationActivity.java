package com.app.deecid.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.Helper.Utils;
import com.app.deecid.R;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.VolleyCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OTPVerficationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    LinearLayout verifiedLayout, valueLayout, codeCall;
    Button nextButton;
    String mobileNumber, OTP, type, countrycode;
    EditText otp_edit_one, otp_edit_two, otp_edit_three, otp_edit_four;
    TextView phoneNumber;
    AppSettings appSettings = new AppSettings(OTPVerficationActivity.this);
    private boolean isVerified;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String latitudeValue, longitudeValue;
    ImageView editIcons, backButton;
    private String TAG = OTPVerficationActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_otpverfication);
        initViews();
        initListners();

        initlocation();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            settingsrequest();
            initlocationRequest();
        }
    }


    public boolean checkGpsisEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {


            case 2:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    initlocationRequest();
                    break;
                } else {

                    break;

                }

            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initlocationRequest();
                    settingsrequest();


                }
                break;


            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }


    private void initlocation() {
        checkCallingOrSelfPermission("");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }


    private void initlocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(100 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);

    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(OTPVerficationActivity.this, 5);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    private void initListners() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String otpOne = otp_edit_one.getText().toString().trim();
                String otpTwo = otp_edit_two.getText().toString().trim();
                String otpThree = otp_edit_three.getText().toString().trim();
                String otpFour = otp_edit_four.getText().toString().trim();
                if (otpOne.isEmpty() && otpTwo.isEmpty() && otpThree.isEmpty() && otpFour.isEmpty()) {
                    Toast.makeText(OTPVerficationActivity.this, R.string.enter_your_code, Toast.LENGTH_SHORT).show();

                } else if (otpOne.isEmpty()) {
                    Toast.makeText(OTPVerficationActivity.this, R.string.enter_your_code, Toast.LENGTH_SHORT).show();
                } else if (otpTwo.isEmpty()) {
                    Toast.makeText(OTPVerficationActivity.this, R.string.enter_your_code, Toast.LENGTH_SHORT).show();
                } else if (otpThree.isEmpty()) {
                    Toast.makeText(OTPVerficationActivity.this, R.string.enter_your_code, Toast.LENGTH_SHORT).show();
                } else if (otpFour.isEmpty()) {
                    Toast.makeText(OTPVerficationActivity.this, R.string.enter_your_code, Toast.LENGTH_SHORT).show();
                } else {
                    OTP = otpOne + otpTwo + otpThree + otpFour;
                    try {
                        verifyOtp(OTP);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        });

        codeCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mobileNumber.isEmpty() && !countrycode.isEmpty()) {
                    setCodeCall(countrycode, mobileNumber);
                }
            }
        });


        editIcons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        otp_edit_one.addTextChangedListener(new GenericTextWatcher(otp_edit_one));
        otp_edit_two.addTextChangedListener(new GenericTextWatcher(otp_edit_two));
        otp_edit_three.addTextChangedListener(new GenericTextWatcher(otp_edit_three));
        otp_edit_four.addTextChangedListener(new GenericTextWatcher(otp_edit_four));
    }

    private void setCodeCall(String countryCode, String mobileNumber) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobileNumber", mobileNumber);
            jsonObject.put("countryCode", countryCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(OTPVerficationActivity.this, UrlHelper.CALL_CODE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.e(TAG, "onSuccess: " + response);

            }
        });
    }

    private void moveInitProfileActivity() {
        appSettings.setMobileNumber(mobileNumber);
        appSettings.setIsLogged("init");
        Intent intent = new Intent(OTPVerficationActivity.this, InitProfileActivity.class);
        intent.putExtra("mobile_number", mobileNumber);
        startActivity(intent);
        finish();

    }

    private void initViews() {
        codeCall = (LinearLayout) findViewById(R.id.codeCall);
        verifiedLayout = (LinearLayout) findViewById(R.id.verifiedLayout);
        valueLayout = (LinearLayout) findViewById(R.id.valueLayout);
        nextButton = (Button) findViewById(R.id.nextButton);
        otp_edit_one = (EditText) findViewById(R.id.otp_edit_one);
        otp_edit_two = (EditText) findViewById(R.id.otp_edit_two);
        otp_edit_three = (EditText) findViewById(R.id.otp_edit_three);
        otp_edit_four = (EditText) findViewById(R.id.otp_edit_four);
        editIcons = (ImageView) findViewById(R.id.editIcons);
        backButton = (ImageView) findViewById(R.id.backButton);
        phoneNumber = (TextView) findViewById(R.id.phoneNumber);
        OTP = getIntent().getStringExtra("otp");
        mobileNumber = getIntent().getStringExtra("mobile_number");
        type = appSettings.getLoginType();


        if (getIntent().getStringExtra("userStatus").equalsIgnoreCase("1")) {
            isVerified = true;
        } else {
            isVerified = false;
        }

        countrycode = getIntent().getStringExtra("countryCode");
        mobileNumber = getIntent().getStringExtra("mobile_number");

        otp_edit_one.setText("" + OTP.charAt(0));
        otp_edit_two.setText("" + OTP.charAt(1));
        otp_edit_three.setText("" + OTP.charAt(2));
        otp_edit_four.setText("" + OTP.charAt(3));

        phoneNumber.setText(mobileNumber);


    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {

            }
        } else {

            latitudeValue = "" + location.getLatitude();
            longitudeValue = "" + location.getLongitude();
            Utils.log(TAG, "mylat:" + latitudeValue + ",mylong:" + longitudeValue);


        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        latitudeValue = "" + location.getLatitude();
        longitudeValue = "" + location.getLongitude();


        Utils.log(TAG, "mylat:" + latitudeValue + ",mylong:" + longitudeValue);

    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.otp_edit_one:
                    if (text.length() == 1)
                        otp_edit_two.requestFocus();
                    break;
                case R.id.otp_edit_two:
                    if (text.length() == 1)
                        otp_edit_three.requestFocus();
                    break;
                case R.id.otp_edit_three:
                    if (text.length() == 1)
                        otp_edit_four.requestFocus();
                    break;
                case R.id.otp_edit_four:
                    if (text.length() == 1)
                        Utils.hideKeyboard(OTPVerficationActivity.this);
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public void verifyOtp(String otp) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OTP", otp);
        jsonObject.put("mobileNumber", mobileNumber);
        jsonObject.put("countryCode", countrycode);
        ApiCall.PostMethod(OTPVerficationActivity.this, UrlHelper.VERIFY, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                valueLayout.setVisibility(View.GONE);
                verifiedLayout.setVisibility(View.VISIBLE);
                type = appSettings.getLoginType();
                appSettings.setAccessToken(response.optString("accessToken"));
                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (type.equalsIgnoreCase("fb")) {


                            if (checkGpsisEnabled()) {
                                try {
                                    uploadData();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                                } else {
                                    settingsrequest();
                                    initlocationRequest();
                                }
                            }


                        } else {
                            if (isVerified) {
                                try {
                                    updateToken();
                                    getProfileData();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                moveInitProfileActivity();
                            }
                        }

                    }
                }, 3000);
            }
        });
    }

    private void updateToken() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("deviceToken", appSettings.getFireBaseToken());
        jsonObject.put("os", "android");
        ApiCall.PostMethodHeaders(OTPVerficationActivity.this, UrlHelper.DEVICE_TOKEN, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });
    }


    private void uploadData() throws JSONException {
        JSONObject jsonObject = new JSONObject(appSettings.getFbDetails());
        jsonObject.put("mobileNumber", mobileNumber);
        jsonObject.put("locLattitude", latitudeValue);
        jsonObject.put("locLongitude", longitudeValue);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("genderPreference", "sugar_baby_boy");
        jsonObject.put("userdatingterms", " ");
        ApiCall.PostMethod(OTPVerficationActivity.this, UrlHelper.PROFILE_REGISTER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                try {
                    updateToken();

                    getProfileData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getProfileData() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(OTPVerficationActivity.this, UrlHelper.VIEW_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray jsonArray = response.optJSONArray("results");
                JSONObject object = jsonArray.optJSONObject(0);
                appSettings.setIsLogged("true");
                appSettings.setUserId(object.optString("id"));
                appSettings.setUserName(object.optString("userName"));
                try {
                    appSettings.setUserImageUrl(object.optJSONArray("images").optJSONObject(0).optString("imageURL"));
                } catch (Exception e) {

                }
                appSettings.setAboutUs(object.optString("aboutUser"));
                appSettings.setJobTitle(object.optString("jobTitle"));
                appSettings.setCompany(object.optString("company"));
                appSettings.setCollege(object.optString("school"));
                appSettings.setGender(object.optString("gender"));
                appSettings.setImagesArray(object.optJSONArray("images"));
                appSettings.setShowAge(object.optString("dontShowAge"));
                appSettings.setAge(object.optString("age"));
                appSettings.setShowDistance(object.optString("makeDistanceInvisible"));
                if (object.optString("InstaStatus").equalsIgnoreCase("connect")) {
                    appSettings.setIsInstagramConnected("true");
                    appSettings.setInstagramName(object.optString("InstaName"));
                    appSettings.setInstagramToken(object.optString("InstaToken"));
                    appSettings.setInstagramId(object.optString("InstaSenderID"));
                } else {
                    appSettings.setInstagramName("");
                    appSettings.setTotalCount("0");
                    appSettings.setIsInstagramConnected("false");
                    JSONArray jsonArrays = new JSONArray();
                    appSettings.setInstaGramPhotos(jsonArrays);
                }
                moveMainActivity();


            }
        });
    }

    private void moveMainActivity() {
        Intent intent = new Intent(OTPVerficationActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("tab", "0");
        startActivity(intent);
    }

}


