package com.app.deecid.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.Helper.Utils;
import com.app.deecid.R;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.VolleyCallback;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.makeramen.roundedimageview.RoundedImageView;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.Random;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InitProfileActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    ImageView imageOne, imageTwo, imageThree, imageFour, imageFive, imageSix, imageSeven,
            imageEight, backButton, maleImage, femaleImage, boyImage, girlImage;
    LinearLayout emailLayout, passwordLayout, nameLayout, birthDayLayout, genderLayout, photoLayout,
            sugarLayout, sugarPref;
    private Button emailNextButton, passwordNextButton, nameNextButton, birthdayNextButton, genderNextButton, doneButton,
            sugarNext, sugarPrefNext;

    private EditText registerEmail, userNameRegister, dateRegister, monthRegister, yearRegister;
    private CardView menLayout, womenLayout, sugarAdult, sugarBaby, sugarPrefBaby, sugarPrefAdult;
    private TextView sugarBabyTv, sugarAdultTv, sugarPrefBabyTv, sugarPrefAdultTv;
    RelativeLayout chooseImage;
    LinearLayout addPhoto;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    int page = 1;
    RoundedImageView profileImage;
    private Uri uri;
    private String displayPicUrl, path = "";
    AppSettings appSettings = new AppSettings(InitProfileActivity.this);
    private File displayPicture;
    String emailValue, dateofBirthValue, userNameValue, mobileNumberValue, genderValue = "male", latitudeValue, longitudeValue, finalImageUrl;
    private String TAG = InitProfileActivity.class.getSimpleName();
    private String filePath;
    private String userDatingTerms;
    private EditText dating_terms;
    private String genderPreference;
    private String sugarGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_profile);
        initViews();
        initListners();
        setPageOne("front");
        initlocation();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            settingsrequest();
            initlocationRequest();
        }


    }

    private void initlocation() {
        checkCallingOrSelfPermission("");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }


    private void initlocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);

    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(InitProfileActivity.this, 5);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    public void setEnterExitViewAnimation(View out, View in, String front) {
        if (front.equalsIgnoreCase("front")) {
            in.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_from_right);
            Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.slide_to_left);
            in.startAnimation(animation);
            out.startAnimation(animation1);
        } else {
            in.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_from_right);
            Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.slide_to_left);
            in.startAnimation(animation);
            out.startAnimation(animation1);
        }

    }


    private void initListners() {
        emailNextButton.setOnClickListener(this);
        passwordNextButton.setOnClickListener(this);
        nameNextButton.setOnClickListener(this);
        genderNextButton.setOnClickListener(this);
        sugarNext.setOnClickListener(this);
        sugarPrefNext.setOnClickListener(this);
        birthdayNextButton.setOnClickListener(this);
        doneButton.setOnClickListener(this);
        menLayout.setOnClickListener(this);
        womenLayout.setOnClickListener(this);
        sugarBaby.setOnClickListener(this);
        sugarAdult.setOnClickListener(this);
        sugarPrefBaby.setOnClickListener(this);
        sugarPrefAdult.setOnClickListener(this);
        chooseImage.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    private void setPageEight(String front) {
        page = 8;

        emailLayout.setVisibility(View.GONE);
        passwordLayout.setVisibility(View.GONE);
        nameLayout.setVisibility(View.GONE);
        birthDayLayout.setVisibility(View.GONE);
        genderLayout.setVisibility(View.GONE);
        sugarLayout.setVisibility(View.GONE);
        sugarPref.setVisibility(View.GONE);
        photoLayout.setVisibility(View.GONE);


        imageOne.setAlpha((float) 0.5);
        imageTwo.setAlpha((float) 0.5);
        imageThree.setAlpha((float) 0.5);
        imageFour.setAlpha((float) 0.5);
        imageFive.setAlpha((float) 0.5);
        imageSix.setAlpha((float) 0.5);
        imageSeven.setAlpha((float) 0.5);
        imageEight.setAlpha((float) 1);

        if (front.equalsIgnoreCase("front")) {
            photoLayout.setVisibility(View.VISIBLE);

            setEnterExitViewAnimation(sugarPref, photoLayout, front);
        } else {
            setExitViewAnimation(sugarPref, photoLayout);

        }
    }

    private void setPageSeven(String front) {
        page = 7;

        emailLayout.setVisibility(View.GONE);
        passwordLayout.setVisibility(View.GONE);
        nameLayout.setVisibility(View.GONE);
        birthDayLayout.setVisibility(View.GONE);
        genderLayout.setVisibility(View.GONE);
        sugarLayout.setVisibility(View.GONE);


        imageOne.setAlpha((float) 0.5);
        imageTwo.setAlpha((float) 0.5);
        imageThree.setAlpha((float) 0.5);
        imageFour.setAlpha((float) 0.5);
        imageFive.setAlpha((float) 0.5);
        imageSix.setAlpha((float) 0.5);
        imageSeven.setAlpha((float) 1);
        imageEight.setAlpha((float) 0.5);

        if (front.equalsIgnoreCase("front")) {
            sugarPref.setVisibility(View.VISIBLE);

            if (genderValue.equalsIgnoreCase("male") && sugarGender.equalsIgnoreCase("sugar_baby_boy")) {
                sugarPrefBabyTv.setText(R.string.sugar_mommy);
                sugarPrefAdultTv.setText(R.string.sugar_daddy);

                maleImage.setImageResource(R.drawable.female);
                femaleImage.setImageResource(R.drawable.male);

            } else if (genderValue.equalsIgnoreCase("male") && sugarGender.equalsIgnoreCase("sugar_daddy")) {

                sugarPrefBabyTv.setText(R.string.sugar_baby_girl);
                sugarPrefAdultTv.setText(R.string.sugar_baby_boy);

                maleImage.setImageResource(R.drawable.girl);
                femaleImage.setImageResource(R.drawable.boy);

            } else if (genderValue.equalsIgnoreCase("female") && sugarGender.equalsIgnoreCase("sugar_baby_girl")) {
                sugarPrefBabyTv.setText(R.string.sugar_mommy);
                sugarPrefAdultTv.setText(R.string.sugar_daddy);

                maleImage.setImageResource(R.drawable.female);
                femaleImage.setImageResource(R.drawable.male);

            } else if (genderValue.equalsIgnoreCase("female") && sugarGender.equalsIgnoreCase("sugar_mummy")) {

                sugarPrefBabyTv.setText(R.string.sugar_baby_girl);
                sugarPrefAdultTv.setText(R.string.sugar_baby_boy);

                maleImage.setImageResource(R.drawable.girl);
                femaleImage.setImageResource(R.drawable.boy);
            }

            setEnterExitViewAnimation(sugarLayout, sugarPref, front);
        } else {
            setExitViewAnimation(photoLayout, sugarPref);

        }
    }

    private void setPageSix(String front) {
        page = 6;

        emailLayout.setVisibility(View.GONE);
        passwordLayout.setVisibility(View.GONE);
        nameLayout.setVisibility(View.GONE);
        birthDayLayout.setVisibility(View.GONE);
        genderLayout.setVisibility(View.GONE);
        photoLayout.setVisibility(View.GONE);
        sugarPref.setVisibility(View.GONE);
        imageOne.setAlpha((float) 0.5);
        imageTwo.setAlpha((float) 0.5);
        imageThree.setAlpha((float) 0.5);
        imageFour.setAlpha((float) 0.5);
        imageFive.setAlpha((float) 0.5);
        imageSix.setAlpha((float) 1);
        imageSeven.setAlpha((float) 0.5);
        imageEight.setAlpha((float) 0.5);
        if (front.equalsIgnoreCase("front")) {
            sugarLayout.setVisibility(View.VISIBLE);

            if (genderValue.equalsIgnoreCase("male")) {
                sugarBabyTv.setText(R.string.sugar_baby_boy);
                sugarAdultTv.setText(R.string.sugar_daddy);

                boyImage.setImageResource(R.drawable.boy);
                girlImage.setImageResource(R.drawable.male);

            } else {

                sugarBabyTv.setText(R.string.sugar_baby_girl);
                sugarAdultTv.setText(R.string.sugar_mommy);

                boyImage.setImageResource(R.drawable.girl);
                girlImage.setImageResource(R.drawable.female);
            }

            setEnterExitViewAnimation(genderLayout, sugarLayout, front);
        } else {
            setExitViewAnimation(sugarPref, sugarLayout);

        }


    }


    private void setPageFive(String front) {
        page = 5;

        emailLayout.setVisibility(View.GONE);
        passwordLayout.setVisibility(View.GONE);
        nameLayout.setVisibility(View.GONE);
        birthDayLayout.setVisibility(View.GONE);
        photoLayout.setVisibility(View.GONE);
        sugarLayout.setVisibility(View.GONE);
        sugarPref.setVisibility(View.GONE);
        imageOne.setAlpha((float) 0.5);
        imageTwo.setAlpha((float) 0.5);
        imageThree.setAlpha((float) 0.5);
        imageFour.setAlpha((float) 0.5);
        imageFive.setAlpha((float) 1);
        imageSix.setAlpha((float) 0.5);
        imageSeven.setAlpha((float) 0.5);
        imageEight.setAlpha((float) 0.5);
        if (front.equalsIgnoreCase("front")) {

            genderLayout.setVisibility(View.VISIBLE);
            setEnterExitViewAnimation(birthDayLayout, genderLayout, front);

        } else {
            setExitViewAnimation(sugarLayout, genderLayout);

        }


    }

    private void setPageFour(String front) {
        page = 4;

        emailLayout.setVisibility(View.GONE);
        passwordLayout.setVisibility(View.GONE);
        nameLayout.setVisibility(View.GONE);
        genderLayout.setVisibility(View.GONE);
        photoLayout.setVisibility(View.GONE);
        sugarLayout.setVisibility(View.GONE);
        sugarPref.setVisibility(View.GONE);
        imageOne.setAlpha((float) 0.5);
        imageTwo.setAlpha((float) 0.5);
        imageThree.setAlpha((float) 0.5);
        imageFour.setAlpha((float) 1);
        imageFive.setAlpha((float) 0.5);
        imageSix.setAlpha((float) 0.5);
        imageSeven.setAlpha((float) 0.5);
        imageEight.setAlpha((float) 0.5);
        if (front.equalsIgnoreCase("front")) {
            birthDayLayout.setVisibility(View.VISIBLE);

            setEnterExitViewAnimation(nameLayout, birthDayLayout, front);
            dateRegister.requestFocus();

        } else {
            setExitViewAnimation(genderLayout, birthDayLayout);

        }


    }


    private void setPageThree(String front) {
        page = 3;

        emailLayout.setVisibility(View.GONE);
        passwordLayout.setVisibility(View.GONE);
        birthDayLayout.setVisibility(View.GONE);
        genderLayout.setVisibility(View.GONE);
        photoLayout.setVisibility(View.GONE);
        sugarLayout.setVisibility(View.GONE);
        sugarPref.setVisibility(View.GONE);
        imageOne.setAlpha((float) 0.5);
        imageTwo.setAlpha((float) 0.5);
        imageThree.setAlpha((float) 1);
        imageFour.setAlpha((float) 0.5);
        imageFive.setAlpha((float) 0.5);
        imageSix.setAlpha((float) 0.5);
        imageSeven.setAlpha((float) 0.5);
        imageEight.setAlpha((float) 0.5);
        if (front.equalsIgnoreCase("front")) {
            nameLayout.setVisibility(View.VISIBLE);

            setEnterExitViewAnimation(passwordLayout, nameLayout, front);
            userNameRegister.requestFocus();


        } else {
            setExitViewAnimation(birthDayLayout, nameLayout);

        }

    }

    private void setPageTwo(String front) {
        page = 2;
        emailLayout.setVisibility(View.GONE);
        nameLayout.setVisibility(View.GONE);
        birthDayLayout.setVisibility(View.GONE);
        genderLayout.setVisibility(View.GONE);
        photoLayout.setVisibility(View.GONE);
        sugarLayout.setVisibility(View.GONE);
        sugarPref.setVisibility(View.GONE);
        imageOne.setAlpha((float) 0.5);
        imageTwo.setAlpha((float) 1);
        imageThree.setAlpha((float) 0.5);
        imageFour.setAlpha((float) 0.5);
        imageFive.setAlpha((float) 0.5);
        imageSix.setAlpha((float) 0.5);
        imageSeven.setAlpha((float) 0.5);
        imageEight.setAlpha((float) 0.5);

        if (front.equalsIgnoreCase("front")) {
            passwordLayout.setVisibility(View.VISIBLE);

            setEnterExitViewAnimation(emailLayout, passwordLayout, front);

        } else {
            setExitViewAnimation(nameLayout, passwordLayout);

        }


    }

    private void setPageOne(String back) {
        page = 1;

        passwordLayout.setVisibility(View.GONE);
        nameLayout.setVisibility(View.GONE);
        birthDayLayout.setVisibility(View.GONE);
        genderLayout.setVisibility(View.GONE);
        photoLayout.setVisibility(View.GONE);
        sugarLayout.setVisibility(View.GONE);
        sugarPref.setVisibility(View.GONE);
        imageOne.setAlpha((float) 1);
        imageTwo.setAlpha((float) 0.5);
        imageThree.setAlpha((float) 0.5);
        imageFour.setAlpha((float) 0.5);
        imageFive.setAlpha((float) 0.5);
        imageSix.setAlpha((float) 0.5);
        imageSeven.setAlpha((float) 0.5);
        imageEight.setAlpha((float) 0.5);

        if (back.equalsIgnoreCase("back")) {

            setExitViewAnimation(passwordLayout, emailLayout);


        } else {
            emailLayout.setVisibility(View.VISIBLE);

        }

    }

    private void setExitViewAnimation(View out, final View in) {

        in.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_to_right);
        Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.slide_from_left);
        in.startAnimation(animation1);
        out.startAnimation(animation);


    }

    private void initViews() {

        imageOne = (ImageView) findViewById(R.id.imageOne);
        imageTwo = (ImageView) findViewById(R.id.imageTwo);
        imageThree = (ImageView) findViewById(R.id.imageThree);
        imageFour = (ImageView) findViewById(R.id.imageFour);
        imageFive = (ImageView) findViewById(R.id.imageFive);
        imageSix = (ImageView) findViewById(R.id.imageSix);
        imageSeven = (ImageView) findViewById(R.id.imageSeven);
        imageEight = (ImageView) findViewById(R.id.imageEight);

        maleImage = (ImageView) findViewById(R.id.maleImage);
        femaleImage = (ImageView) findViewById(R.id.femaleImage);
        boyImage = (ImageView) findViewById(R.id.boyImage);
        girlImage = (ImageView) findViewById(R.id.girlImage);

        sugarBabyTv = (TextView) findViewById(R.id.sugarBabyTv);
        sugarAdultTv = (TextView) findViewById(R.id.sugarAdultTv);
        sugarPrefBabyTv = (TextView) findViewById(R.id.sugarPrefBabyTv);
        sugarPrefAdultTv = (TextView) findViewById(R.id.sugarPrefAdultTv);
        dating_terms = (EditText) findViewById(R.id.dating_terms);


        emailLayout = (LinearLayout) findViewById(R.id.emailLayout);
        passwordLayout = (LinearLayout) findViewById(R.id.passwordLayout);
        nameLayout = (LinearLayout) findViewById(R.id.nameLayout);
        birthDayLayout = (LinearLayout) findViewById(R.id.birthDayLayout);
        genderLayout = (LinearLayout) findViewById(R.id.genderLayout);
        photoLayout = (LinearLayout) findViewById(R.id.photoLayout);
        sugarLayout = (LinearLayout) findViewById(R.id.sugarLayout);
        sugarPref = (LinearLayout) findViewById(R.id.sugarPref);

        emailNextButton = (Button) findViewById(R.id.emailNextButton);
        passwordNextButton = (Button) findViewById(R.id.passwordNextButton);
        nameNextButton = (Button) findViewById(R.id.nameNextButton);
        genderNextButton = (Button) findViewById(R.id.genderNextButton);
        sugarNext = (Button) findViewById(R.id.sugarNext);
        sugarPrefNext = (Button) findViewById(R.id.sugarPrefNext);
        birthdayNextButton = (Button) findViewById(R.id.birthdayNextButton);
        doneButton = (Button) findViewById(R.id.doneButton);


        womenLayout = (CardView) findViewById(R.id.womenLayout);
        menLayout = (CardView) findViewById(R.id.menLayout);
        sugarBaby = (CardView) findViewById(R.id.sugarBaby);
        sugarAdult = (CardView) findViewById(R.id.sugarAdult);
        sugarPrefBaby = (CardView) findViewById(R.id.sugarPrefBaby);
        sugarPrefAdult = (CardView) findViewById(R.id.sugarPrefAdult);

        chooseImage = (RelativeLayout) findViewById(R.id.chooseImage);
        addPhoto = (LinearLayout) findViewById(R.id.addPhoto);
        profileImage = (RoundedImageView) findViewById(R.id.profileImage);
        backButton = (ImageView) findViewById(R.id.backButton);


        registerEmail = (EditText) findViewById(R.id.registerEmail);
        userNameRegister = (EditText) findViewById(R.id.userNameRegister);
        dateRegister = (EditText) findViewById(R.id.dateRegister);
        monthRegister = (EditText) findViewById(R.id.monthRegister);
        yearRegister = (EditText) findViewById(R.id.yearRegister);

        dateRegister.addTextChangedListener(new GenericTextWatcher(dateRegister));
        monthRegister.addTextChangedListener(new GenericTextWatcher(monthRegister));
        yearRegister.addTextChangedListener(new GenericTextWatcher(yearRegister));
        mobileNumberValue = getIntent().getStringExtra("mobile_number");

    }


    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.dateRegister:
                    if (text.length() == 2)
                        monthRegister.requestFocus();
                    break;
                case R.id.monthRegister:
                    if (text.length() == 2)
                        yearRegister.requestFocus();
                    break;

                case R.id.yearRegister:
                    if (text.length() == 4)
                        Utils.hideKeyboard(InitProfileActivity.this);
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onClick(View view) {
        if (view == emailNextButton) {
            emailValue = registerEmail.getText().toString();
            if (emailValue.length() == 0) {
                Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_email_value));
            } else {
                if (Utils.isValidEmail(emailValue)) {
                    setPageThree("front");
                } else {
                    Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_email));

                }
            }
        } else if (view == passwordNextButton) {
//            setPageThree("front");
        } else if (view == nameNextButton) {

            userNameValue = userNameRegister.getText().toString();
            if (userNameValue.length() == 0) {
                Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_first_name));

            } else {
                setPageFour("front");
            }
        } else if (view == genderNextButton) {
            setSugarBabySelected();
            setPageSix("front");

        } else if (view == sugarNext) {
            setSugarPrefBabySelected();
            setPageSeven("front");


        } else if (view == sugarPrefNext) {
            if (dating_terms.getText().toString().trim().length() != 0) {
                userDatingTerms = dating_terms.getText().toString().trim();
                Log.e(TAG, "Gender:" + genderValue + ", sugarGender: " + sugarGender + ", genderPref: " + genderPreference);
                setPageEight("front");
            } else {
                Toast.makeText(this, getString(R.string.dating_terms), Toast.LENGTH_SHORT).show();
            }

        } else if (view == birthdayNextButton) {
            if (isValidDataOfBith()) {
                dateofBirthValue = yearRegister.getText().toString() + "-" + monthRegister.getText().toString() + "-" + dateRegister.getText().toString();
                setPageFive("front");
            }

        } else if (view == doneButton) {
            Utils.show(InitProfileActivity.this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (doesUserHavePermission()) {
                    if (checkGpsisEnabled()) {
                        if (displayPicture != null) {
                            Utils.show(InitProfileActivity.this);
                            uploadImage();
                        } else {
                            Utils.dismiss(InitProfileActivity.this);

                            Utils.toast(InitProfileActivity.this, getResources().getString(R.string.kindly_upload_a_prof));
                        }
                    } else {
                        Utils.dismiss(InitProfileActivity.this);

                        settingsrequest();
                        initlocationRequest();
                    }
                } else {
                    Utils.dismiss(InitProfileActivity.this);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                }
            } else {

                if (checkGpsisEnabled()) {
                    if (displayPicture != null) {
                        Utils.show(InitProfileActivity.this);
                        uploadImage();
                    } else {
                        Utils.dismiss(InitProfileActivity.this);

                        Utils.toast(InitProfileActivity.this, getResources().getString(R.string.kindly_upload_a_prof));
                    }
                } else {
                    Utils.dismiss(InitProfileActivity.this);

                    settingsrequest();
                    initlocationRequest();
                }
            }


        } else if (view == womenLayout) {
            setWomenSelected();

        } else if (view == menLayout) {
            setMenSelected();
        } else if (view == sugarBaby) {
            setSugarBabySelected();

        } else if (view == sugarAdult) {
            setSugarAdultSelected();

        } else if (view == sugarPrefBaby) {
            setSugarPrefBabySelected();

        } else if (view == sugarPrefAdult) {
            setSugarPrefAdultSelected();

        } else if (view == chooseImage) {
            showPictureDialog();
        } else if (view == backButton) {
            goBack();
        }


    }


    private boolean doesUserHavePermission() {
        int result = checkCallingOrSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }


    public boolean checkGpsisEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidDataOfBith() {
        String yearReg = yearRegister.getText().toString().trim();
        String monthReg = monthRegister.getText().toString().trim();
        String dateReg = dateRegister.getText().toString().trim();

        int year;
        int month;
        int date;

        if (!yearReg.isEmpty() && !monthReg.isEmpty() && !dateReg.isEmpty()) {
            year = Integer.parseInt(yearReg);
            month = Integer.parseInt(monthReg);
            date = Integer.parseInt(dateReg);

            if (yearReg.length() != 4 || year < 1971 || year > 2017) {
                Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_year));
                return false;
            } else if (getAge(year, month, date) < 18) {
                Utils.toast(InitProfileActivity.this, getResources().getString(R.string.minimum_age));
                return false;

            } else if (monthReg.length() != 2 || month <= 0 || month > 12) {
                Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_month));
                return false;
            } else if (dateReg.length() != 2 || date < 0 || date > 31) {
                Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_date));
                return false;

            } else if (dateReg.equals("00")) {
                Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_date));
                return false;

            } else if (monthReg.equals("00")) {
                Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_date));
                return false;

            } else if (monthReg.equals("02")) {

                if (dateReg.equals("30") || dateReg.equals("31")) {
                    Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_month));
                    return false;

                } else {
                    return true;
                }

            } else {
                return true;
            }
        } else if (dateReg.isEmpty()) {
            Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_date));
            return false;

        } else if (monthReg.isEmpty()) {
            Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_month));
            return false;

        } else if (yearReg.isEmpty()) {
            Utils.toast(InitProfileActivity.this, getResources().getString(R.string.please_enter_valid_year));
            return false;

        } else {
            return false;
        }

    }

    private int getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageInt;
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        if (page == 1) {
            movePhoneNumberActivity();
        } else if (page == 2) {
            setPageOne("back");
        } else if (page == 3) {
//            setPageTwo("back");
            setPageOne("back");
        } else if (page == 4) {
            setPageThree("back");
        } else if (page == 5) {
            setPageFour("back");
        } else if (page == 6) {
            setPageFive("back");
        } else if (page == 7) {
            setPageSix("back");
        } else if (page == 8) {
            setPageSeven("back");
        }
    }

    private void movePhoneNumberActivity() {
        Intent intent = new Intent(InitProfileActivity.this, PhoneNumberActivity.class);
        startActivity(intent);
    }


    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 100);

    }

    public void uploadImage() {
        UploadImage();
//        Utils.show(InitProfileActivity.this);
//        ApiCall.uploadImage(displayPicture, InitProfileActivity.this, new VolleyCallback() {
//            @Override
//            public void onSuccess(JSONObject response) {
//                Log.d(TAG, "onSuccess: " + response);
//                Utils.dismiss(InitProfileActivity.this);
//                finalImageUrl = response.optString("URL");
//                try {
//                    registerUserDetails();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
    }

    public void UploadImage() {

        new uploadToserver().execute();
    }

    public class uploadToserver extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            Utils.show(InitProfileActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {

            ApiCall.uploadImage(displayPicture, InitProfileActivity.this, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d(TAG, "onSuccess: " + response);

                    finalImageUrl = response.optString("URL");

                }
            });
            return null;
        }

        protected void onPostExecute(Void result) {
            Utils.dismiss(InitProfileActivity.this);

            if (finalImageUrl != null) {
                try {
                    registerUserDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(InitProfileActivity.this, R.string.image_failed, Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void registerUserDetails() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", emailValue);
        jsonObject.put("dob", dateofBirthValue);
        jsonObject.put("userName", userNameValue);
        jsonObject.put("gender", sugarGender);
        jsonObject.put("genderPreference", genderPreference);
        jsonObject.put("userdatingterms", userDatingTerms);
        jsonObject.put("imageURL", finalImageUrl);
        jsonObject.put("mobileNumber", mobileNumberValue);
        jsonObject.put("locLattitude", latitudeValue);
        jsonObject.put("locLongitude", longitudeValue);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        Log.d(TAG, "registerUserDetails: " + jsonObject);

        ApiCall.PostMethod(InitProfileActivity.this, UrlHelper.PROFILE_REGISTER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);

                try {
                    updateToken();
                    getProfileData();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

    }


    private void updateToken() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("deviceToken", appSettings.getFireBaseToken());
        jsonObject.put("os", "android");
        ApiCall.PostMethodHeaders(InitProfileActivity.this, UrlHelper.DEVICE_TOKEN, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });
    }


    private void getProfileData() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(InitProfileActivity.this, UrlHelper.VIEW_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray jsonArray = response.optJSONArray("results");
                JSONObject object = jsonArray.optJSONObject(0);

                appSettings.setIsLogged("true");
                appSettings.setUserId(object.optString("id"));
                appSettings.setUserImageUrl(object.optJSONArray("images").optJSONObject(0).optString("imageURL"));
                appSettings.setAboutUs(object.optString("aboutUser"));
                appSettings.setJobTitle(object.optString("jobTitle"));
                appSettings.setCompany(object.optString("company"));
                appSettings.setUserName(object.optString("userName"));
                appSettings.setCollege(object.optString("school"));
                appSettings.setImagesArray(object.optJSONArray("images"));
                appSettings.setGender(object.optString("gender"));
                appSettings.setGenderPref(object.optString("genderPreference"));
                appSettings.setAge(object.optString("age"));
                appSettings.setShowAge(object.optString("dontShowAge"));
                appSettings.setShowDistance(object.optString("makeDistanceInvisible"));

                if (object.optString("InstaStatus").equalsIgnoreCase("connect")) {
                    appSettings.setIsInstagramConnected("true");
                    appSettings.setInstagramName(object.optString("InstaName"));
                    appSettings.setInstagramToken(object.optString("InstaToken"));
                    appSettings.setInstagramId(object.optString("InstaSenderID"));
                } else {
                    appSettings.setInstagramName("");
                    appSettings.setTotalCount("0");
                    appSettings.setIsInstagramConnected("false");
                    JSONArray jsonArrays = new JSONArray();
                    appSettings.setInstaGramPhotos(jsonArrays);
                }

                moveMainActivity();


            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                //gallery
                if (resultCode == RESULT_OK) {
                    addPhoto.setVisibility(View.GONE);

                    if (data != null) {
                        uri = data.getData();
                        if (uri != null) {

                            beginCrop(uri);
                        } else {
                            Utils.toast(InitProfileActivity.this, getString(R.string.unable_to_select));
                        }
                    }

                }
                break;


            case 104:
                //camera
                if (resultCode == RESULT_OK) {
                    addPhoto.setVisibility(View.GONE);


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        if (uri != null) {
                            beginCrop(uri);
                        } else {
                            Utils.toast(InitProfileActivity.this, getString(R.string.unable_to_select));
                        }

                    } else {
                        Log.d(TAG, "onActivityResult: " + appSettings.getImageValue());
                        if (appSettings.getImageValue().trim().length() != 0) {
                            File file = new File(appSettings.getImageValue());
                            beginCrop(Uri.fromFile(file));
                        } else {
                            Utils.toast(InitProfileActivity.this, getString(R.string.unable_to_select));
                        }

                    }
                }
                break;

            case Crop.REQUEST_CROP:

                if (data != null)
                    handleCrop(resultCode, data);
                break;


        }
    }


    private void beginCrop(Uri source) {
        //String register_id = code + number;

//        Uri outputUri= FileProvider.getUriForFile(InitProfileActivity.this,getPackageName()+".provider",new File(Environment
//                .getExternalStorageDirectory(), Calendar.getInstance().getTimeInMillis() + ".jpg"));

//        Log.d(TAG, "beginCrop: "+outputUri+",hj:"+source);

        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), Calendar.getInstance().getTimeInMillis() + ".jpg"));
        Crop.of(source, outputUri).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {

        if (resultCode == RESULT_OK) {
            //filePath = getRealPathFromURI(Crop.getOutput(result));
            filePath = Utils.getRealPathFromUriNew(this, Crop.getOutput(result));
            //profileImage.setImageURI(Crop.getOutput(result));
            Log.e("image_path", "" + filePath);

            displayPicture = new File(filePath);

            Glide.with(this).load(Crop.getOutput(result)).into(profileImage);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Utils.toast(InitProfileActivity.this, getResources().getString(R.string.unable_to));
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(InitProfileActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 2:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    initlocationRequest();
                    break;
                } else {

                    break;

                }

            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initlocationRequest();
                    settingsrequest();

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(InitProfileActivity.this, getResources().getString(R.string.storage_permission_error));

                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    private void takePhotoFromCamera() {
        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(),
                getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder,
                getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }


        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

//        uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(InitProfileActivity.this, getPackageName() + ".provider", file);
            Log.d(TAG, "onClick: " + uri.getPath());
            appSettings.setImagePath(uri.getPath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, 104);
        } else {
            uri = Uri.fromFile(file);

            path = file.getPath();
            appSettings.setImageValue(path);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, 104);
        }
    }

    private void setMenSelected() {
        genderValue = getString(R.string.male_text);
        womenLayout.setAlpha((float) 0.5);
        menLayout.setAlpha((float) 1);
    }

    private void setWomenSelected() {
        genderValue = getString(R.string.female_text);
        menLayout.setAlpha((float) 0.5);
        womenLayout.setAlpha((float) 1);
    }

    private void setSugarBabySelected() {

        if (genderValue.equalsIgnoreCase("male")) {
            sugarGender = "sugar_baby_boy";

        } else if (genderValue.equalsIgnoreCase("female")) {
            sugarGender = "sugar_baby_girl";

        }
        sugarAdult.setAlpha((float) 0.5);
        sugarBaby.setAlpha((float) 1);
    }

    private void setSugarAdultSelected() {
        if (genderValue.equalsIgnoreCase("male")) {
            sugarGender = "sugar_daddy";

        } else if (genderValue.equalsIgnoreCase("female")) {
            sugarGender = "sugar_mummy";

        }

        sugarBaby.setAlpha((float) 0.5);
        sugarAdult.setAlpha((float) 1);
    }

    private void setSugarPrefBabySelected() {

        if (genderValue.equalsIgnoreCase("male") && sugarGender.equalsIgnoreCase("sugar_baby_boy")) {
            genderPreference = "sugar_mummy";

        } else if (genderValue.equalsIgnoreCase("male") && sugarGender.equalsIgnoreCase("sugar_daddy")) {

            genderPreference = "sugar_baby_girl";

        } else if (genderValue.equalsIgnoreCase("female") && sugarGender.equalsIgnoreCase("sugar_baby_girl")) {
            genderPreference = "sugar_mummy";

        } else if (genderValue.equalsIgnoreCase("female") && sugarGender.equalsIgnoreCase("sugar_mummy")) {

            genderPreference = "sugar_baby_girl";
        }

        sugarPrefAdult.setAlpha((float) 0.5);
        sugarPrefBaby.setAlpha((float) 1);
    }

    private void setSugarPrefAdultSelected() {
        if (genderValue.equalsIgnoreCase("male") && sugarGender.equalsIgnoreCase("sugar_baby_boy")) {
            genderPreference = "sugar_daddy";

        } else if (genderValue.equalsIgnoreCase("male") && sugarGender.equalsIgnoreCase("sugar_daddy")) {

            genderPreference = "sugar_baby_boy";

        } else if (genderValue.equalsIgnoreCase("female") && sugarGender.equalsIgnoreCase("sugar_baby_girl")) {
            genderPreference = "sugar_daddy";

        } else if (genderValue.equalsIgnoreCase("female") && sugarGender.equalsIgnoreCase("sugar_mummy")) {

            genderPreference = "sugar_baby_boy";
        }

        sugarPrefBaby.setAlpha((float) 0.5);
        sugarPrefAdult.setAlpha((float) 1);
    }


    private void moveMainActivity() {
        Intent intent = new Intent(InitProfileActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitudeValue = "" + location.getLatitude();
        longitudeValue = "" + location.getLongitude();
        Utils.log(TAG, "mylat:" + latitudeValue + ",mylong:" + longitudeValue);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {

            }
        } else {

            latitudeValue = "" + location.getLatitude();
            longitudeValue = "" + location.getLongitude();
            Utils.log(TAG, "mylat:" + latitudeValue + ",mylong:" + longitudeValue);


        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }

}
