package com.app.deecid.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.app.deecid.Activity.ChatActivity;
import com.app.deecid.Activity.MainActivity;
import com.app.deecid.R;

import org.json.JSONObject;

/**
 * Created by user on 25-10-2017.
 */

public class FcmMessageService extends FirebaseMessagingService {

    private static final String TAG = FcmMessageService.class.getSimpleName();


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        Log.e(TAG, "onMessageReceived: " + jsonObject);
        sendNotification(jsonObject);


    }

    private void sendNotification(JSONObject jsonObject) {
        Intent intent;

        if (jsonObject.optString("notification_type").equalsIgnoreCase("chat")) {
            intent = new Intent(this, ChatActivity.class);
            intent.putExtra("type","push");

//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("receiverId", jsonObject.optString("sender_id"));


        } else if (jsonObject.optString("notification_type").equalsIgnoreCase("match")) {


            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("tab", "1");

        } else {
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("type", "success");
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.heart)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentTitle(jsonObject.optString("title"))
                .setContentText(jsonObject.optString("body"))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


}