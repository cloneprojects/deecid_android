package com.app.deecid.Services;

import org.json.JSONObject;

/**
 * Created by yuvaraj on 02/12/17.
 */

public class MessageEvent {
    JSONObject jsonObject;

    public MessageEvent() {
    }

    public MessageEvent(JSONObject jsonObject) {

        this.jsonObject = jsonObject;
    }

    public JSONObject getJsonObject() {

        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}
