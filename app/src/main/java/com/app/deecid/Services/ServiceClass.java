package com.app.deecid.Services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by yuvaraj on 02/12/17.
 */

public class ServiceClass extends Service {
    private String my_id;
    private Handler handleroffline = new Handler();
    AppSettings appSettings = new AppSettings(ServiceClass.this);
    public static Socket socket;
    Boolean UpdatedOff = false;
    private boolean Updatedon = false;
    Handler handler=new Handler();
    private Runnable statusCheck = new Runnable() {
        @Override
        public void run() {
            Boolean isBacgkround = isAppIsInBackground(ServiceClass.this);
            if (isBacgkround) {
                if (!UpdatedOff) {
                    Emitters emitters = new Emitters(ServiceClass.this);
                    emitters.emitoffline();
                    UpdatedOff = true;
                    Updatedon = false;
                }
            } else {
                if (!Updatedon) {
                    Emitters emitters = new Emitters(ServiceClass.this);
                    emitters.emitnewonline();
                    UpdatedOff = false;
                    Updatedon = true;
                }
            }
            handleroffline.postDelayed(this, 1000);
        }

    };
    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (String activeProcess : processInfo.pkgList) {
                            if (activeProcess.equals(context.getPackageName())) {
                                isInBackground = false;
                            }
                        }
                    }
                }
            } else {
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                if (componentInfo.getPackageName().equals(context.getPackageName())) {
                    isInBackground = false;
                }
            }
        } catch (Exception e) {

        }

        return isInBackground;

    }


    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            handler.post(statusCheck);

            Emitters emitters = new Emitters(ServiceClass.this);
            emitters.emitonline();

        }

    };
    private static String TAG=ServiceClass.class.getSimpleName();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        my_id = appSettings.getUserId();

        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.reconnection = true;

        Log.d("onCreate: ", "MainActivity");
        try {
            socket = IO.socket(UrlHelper.BASE, opts);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.e("SOCKET.IO ", e.getMessage());
        }
        socket.connect();
        socket.on(Socket.EVENT_CONNECT, onConnect);

        socket.on("recievemessage", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject jsonObject = new JSONObject();
                jsonObject = (JSONObject) args[0];
                Log.d(TAG, "call: "+jsonObject);
                try {
                    jsonObject.put("type","receiver");

                    JSONObject send=new JSONObject();
                    send.put("user_id",appSettings.getUserId());
                    send.put("message_id",jsonObject.optString("message_id"));
                    Emitters emitters=new Emitters(ServiceClass.this);
                    emitters.sendDelievered(send);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                EventBus.getDefault().post(new MessageEvent(jsonObject));
            }
        });


        return START_STICKY;

    }


    public static class Emitters {
        Context context;
        AppSettings appSettings;

        public Emitters(Context context) {
            this.context = context;
            appSettings = new AppSettings(context);
        }


        public void emitonline() {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userid", appSettings.getUserId());
                socket.emit("get_online", jsonObject);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        public void emitoffline() {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userid", appSettings.getUserId());
                socket.emit("isOffline", jsonObject);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        public void emitnewonline() {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userid", appSettings.getUserId());
                socket.emit("isOnline", jsonObject);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        public void sendMessage(JSONObject jsonObject) {
            Log.d(TAG, "sendMessage: "+jsonObject);
            socket.emit("sendmessage", jsonObject);

        }

        public void sendDelievered(JSONObject jsonObject) {

            socket.emit("send_delivered", jsonObject);

        }


    }
}
