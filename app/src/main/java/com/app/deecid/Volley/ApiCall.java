package com.app.deecid.Volley;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.deecid.Application.AppController;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.Helper.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/*
 * Created by user on 23-10-2017.
 */

public class ApiCall {


    public static String strAdd;
    private static String TAG = ApiCall.class.getSimpleName();
    private static int MY_SOCKET_TIMEOUT_MS = 500000;

    public static void getMethod(final Activity context, final String url, final VolleyCallback volleyCallback) {

//        Utils.show(context);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.log(TAG, "url:" + url + ",response: " + response);
//                        Utils.dismiss();
                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                Utils.toast(context, response.optString("message"));
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Utils.dismiss();

                VolleyErrorHandler.handle(url, error);


            }
        });


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }


//    public static void getMethodHeaders(final Context context, final String url, final VolleyCallback volleyCallback) {
//
//        Utils.show(context);
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
//                url,
//                new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Utils.dismiss();
//                        if (response.has("error")) {
//                            if (response.optString("error").equalsIgnoreCase("false")) {
//                                volleyCallback.onSuccess(response);
//                            } else {
//                                Utils.toast(context, response.optString("message"));
//                            }
//                        } else {
//                            volleyCallback.onSuccess(response);
//
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Utils.dismiss();
//
//                VolleyErrorHandler.handle(url, error);
//
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                AppSettings appSettings = new AppSettings(context);
//
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Accept", "application/json");
//                headers.put("Authorization", "Bearer " + appSettings.getToken());
//                return headers;
//            }
//        };
//
//        Uberdoo.getInstance().addToRequestQueue(jsonObjReq);
//
//    }
//

//    public static void getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE, final Callback callback) {
//
//
//        String lat = String.valueOf(LATITUDE);
//        String lngg = String.valueOf(LONGITUDE);
//
//        final String resultt = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + ","
//                + lngg + "&sensor=true&key=" + context.getResources().getString(R.string.google_maps_key);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, resultt, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.d(TAG, "onResponse: " + jsonObject);
//                Log.d(TAG, "resultUrl: " + resultt);
//
//                callback.onSuccess(jsonObject);
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("getjson", "onResponse: " + error);
//
//            }
//        });
//
//        Uberdoo.getInstance().addToRequestQueue(jsonObjectRequest);
//
//    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    public static void PostMethod(final Activity context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        if (isNetworkConnected(context)) {
            Utils.show(context);
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);
                            Utils.dismiss(context);

                            if (response.has("error")) {
                                if (response.optString("error").equalsIgnoreCase("false")) {
                                    volleyCallback.onSuccess(response);
                                } else {

                                    if (!url.equalsIgnoreCase(UrlHelper.MAIN_SCREEN)) {

                                        if (response.has("error_message")) {
                                            Utils.toast(context, response.optString("error_message"));
                                        } else {
                                            Utils.toast(context, response.optString("message"));

                                        }
                                    }
                                }
                            } else {
                                volleyCallback.onSuccess(response);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.dismiss(context);

                    VolleyErrorHandler.handle(url, error);
                }
            });

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        } else {
            Utils.showNoInternet(context);
        }

    }

    public static void PostMethodNoProgress(final Activity context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        if (isNetworkConnected(context)) {
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);

                            if (response.has("error")) {
                                if (response.optString("error").equalsIgnoreCase("false")) {
                                    volleyCallback.onSuccess(response);
                                } else {
                                    if (!url.equalsIgnoreCase(UrlHelper.SWIPE)) {

                                        if (response.has("error_message")) {
                                            Utils.toast(context, response.optString("error_message"));
                                        } else {
                                            Utils.toast(context, response.optString("message"));

                                        }
                                    }
                                }
                            } else {
                                volleyCallback.onSuccess(response);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Utils.log(TAG, "url:" + url + ",response: " + error);

                    VolleyErrorHandler.handle(url, error);
                }
            });

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        } else {
            Utils.showNoInternet(context);
        }

    }


    public static void PostMethodHeaders(final Activity context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        if (isNetworkConnected(context)) {
            Utils.show(context);
            Utils.log(TAG, "url:" + url + ",input: " + params);
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.dismiss(context);
                            Log.d(TAG, "onResponse: " + url + ",response:" + response);
                            if (response.has("error")) {
                                if (response.optString("error").equalsIgnoreCase("false")) {
                                    volleyCallback.onSuccess(response);
                                } else {
                                    Utils.toast(context, response.optString("error_message"));
                                }
                            } else {
                                volleyCallback.onSuccess(response);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.dismiss(context);

                    VolleyErrorHandler.handle(url, error);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    AppSettings appSettings = new AppSettings(context);

                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Accept", "application/json");
//                headers.put("Authorization", "Bearer " + appSettings.getToken());
                    return headers;
                }
            };
//        ;
//        jsonObjReq.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        } else {
            Utils.showNoInternet(context);
        }

    }
//
//
//
//    public static void PostMethodHeadersNoProgress(final Context context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
//        Utils.log(TAG, "url:" + url + ",input: " + params);
//        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, params,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d(TAG, "onResponse: "+url+",response:"+response);
//                        if (response.has("error")) {
//                            if (response.optString("error").equalsIgnoreCase("false")) {
//                                volleyCallback.onSuccess(response);
//                            } else {
//                                Utils.toast(context, response.optString("error_message"));
//                            }
//                        } else {
//                            volleyCallback.onSuccess(response);
//
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                VolleyErrorHandler.handle(url, error);
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                AppSettings appSettings = new AppSettings(context);
//
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Accept", "application/json");
//                headers.put("Authorization", "Bearer " + appSettings.getToken());
//                return headers;
//            }
//        };
////        ;
////        jsonObjReq.setRetryPolicy(new RetryPolicy() {
////            @Override
////            public int getCurrentTimeout() {
////                return 50000;
////            }
////
////            @Override
////            public int getCurrentRetryCount() {
////                return 50000;
////            }
////
////            @Override
////            public void retry(VolleyError error) throws VolleyError {
////
////            }
////        });
//        AppController.getInstance().addToRequestQueue(jsonObjReq);
//
//    }

    public static void uploadImage(File file, Activity context, final VolleyCallback volleyCallback) {
        if (isNetworkConnected(context)) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(30, TimeUnit.SECONDS);
            builder.readTimeout(30, TimeUnit.SECONDS);
            builder.writeTimeout(30, TimeUnit.SECONDS);
            OkHttpClient client = builder.build();
            MediaType MEDIA_TYPE_PNG = MediaType.parse("image/jpeg");

            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file", file.getName(), RequestBody.create(MEDIA_TYPE_PNG, new File(file.getPath())))
                    .build();
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(UrlHelper.IMAGE_UPLOAD)
                    .post(requestBody).build();

            try {
                okhttp3.Response response = client.newCall(request).execute();
                JSONObject jsonObject = new JSONObject(response.body().string());
                volleyCallback.onSuccess(jsonObject);

            } catch (IOException e) {
                Log.e(TAG, "upload_the_image_error: " + e);
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            Utils.showNoInternet(context);
        }
    }


}
