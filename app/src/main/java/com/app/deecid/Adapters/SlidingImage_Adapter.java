package com.app.deecid.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.deecid.R;

/**
 * Created by user on 24-11-2017.
 */
public class SlidingImage_Adapter extends PagerAdapter {


  String [] titles;
  String [] subtitles;
  int [] icons;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImage_Adapter(String[] titles, String[] subtitles, int[] icons, Context context) {
        this.titles = titles;
        this.subtitles = subtitles;
        this.icons = icons;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.dialog_view_pager, view, false);



        TextView text1 = imageLayout.findViewById(R.id.text1);
        TextView text2 = imageLayout.findViewById(R.id.text2);
        ImageView iconsla = imageLayout.findViewById(R.id.icons);

        iconsla.setImageDrawable(context.getResources().getDrawable(icons[position]));
        text1.setText(titles[position]);
        text2.setText(subtitles[position]);

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
