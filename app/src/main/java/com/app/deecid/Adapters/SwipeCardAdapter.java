package com.app.deecid.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.deecid.Fragment.TitlePagerFragment;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.R;
import com.viewpagerindicator.LinePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;


public class SwipeCardAdapter extends BaseAdapter {
    Context context;
    Fragment fragment = null;
    Class fragmentClass = null;
    String suggestId;
    FragmentManager fragmentManager;
    JSONArray jsonArray;
    int mScreenWidth;
    ImageView instagramIcon;
    private String TAG = SwipeCardAdapter.class.getSimpleName();
    private AppSettings appSettings;


    public SwipeCardAdapter(JSONArray jsonArray, Context context, FragmentManager fragmentManager, int mScreenWidth) {
        this.jsonArray = jsonArray;
        this.context = context;
        this.mScreenWidth = mScreenWidth;
        this.fragmentManager = fragmentManager;
        appSettings = new AppSettings(context);
    }

    @Override
    public int getCount() {
        return jsonArray.length();
    }

    @Override
    public Object getItem(int i) {
        return jsonArray.optJSONObject(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_stack_items, viewGroup, false);
        }

        JSONObject jsonObject = jsonArray.optJSONObject(i);

        Log.d(TAG, "getView: " + jsonObject);
        TextView swipeCardComments = (TextView) view.findViewById(R.id.swipeCardComments);
        TextView swipeCardJobTitle = (TextView) view.findViewById(R.id.swipeCardJobTitle);
        TextView swipeCardName = (TextView) view.findViewById(R.id.swipeCardName);
        TextView swipeCardAge = (TextView) view.findViewById(R.id.swipeCardAge);
        ViewPager viewPage = (ViewPager) view.findViewById(R.id.viewPage);
        RelativeLayout bottomLayout = (RelativeLayout) view.findViewById(R.id.bottomLayout);
        final ImageView swipeCardUserImage = (ImageView) view.findViewById(R.id.swipeCardUserImage);
        ImageView superLikeIcon = (ImageView) view.findViewById(R.id.superLikeIcon);
        ImageView instagramIcon = (ImageView) view.findViewById(R.id.instagramIcon);

        if (jsonObject.optString("addStatus").equalsIgnoreCase("false")) {
            swipeCardUserImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

            bottomLayout.setVisibility(View.VISIBLE);

            swipeCardName.setText(jsonObject.optString("userName"));
            if (jsonObject.optString("dontShowAge").equalsIgnoreCase("yes")) {

                swipeCardAge.setText(", " + jsonObject.optString("age"));

            } else {
                swipeCardAge.setText("");

            }
            swipeCardComments.setText(jsonObject.optString("company"));
            //swipeCardJobTitle.setText(jsonObject.optString("jobTitle"));
            swipeCardJobTitle.setText(jsonObject.optString("userdatingterms"));

            if (jsonObject.optString("isSuperLiked").equalsIgnoreCase("1")) {
                Log.d(TAG, "getView: " + jsonObject.optString("isSuperLiked"));
                bottomLayout.setBackground(context.getResources().getDrawable(R.drawable.bottom_blue));
                superLikeIcon.setVisibility(View.VISIBLE);
            } else {
                Log.d(TAG, "getView1: " + jsonObject.optString("isSuperLiked"));

                bottomLayout.setBackground(context.getResources().getDrawable(R.drawable.bottom_black));
                superLikeIcon.setVisibility(View.GONE);

            }


            if (jsonObject.optString("InstaStatus").equalsIgnoreCase("connect")) {
                instagramIcon.setVisibility(View.VISIBLE);

            } else {
                instagramIcon.setVisibility(View.GONE);
            }


            JSONArray imgArray = jsonObject.optJSONArray("profile_images");
            if (imgArray.length() > 0) {
                String imgUrl = imgArray.optJSONObject(0).optString("imageURL");
                Glide.with(context).load(imgUrl).into(swipeCardUserImage);
                ViewPagerAdapter adapter = new ViewPagerAdapter(fragmentManager, imgArray.length());
                viewPage.setAdapter(adapter);

                LinePageIndicator titleIndicator = (LinePageIndicator) view.findViewById(R.id.indicator);
                int le = mScreenWidth / adapter.getCount();
                titleIndicator.setLineWidth(le);
                titleIndicator.setViewPager(viewPage);

            }
        } else {
            swipeCardUserImage.setScaleType(ImageView.ScaleType.FIT_XY);
            bottomLayout.setVisibility(View.GONE);
            Glide.with(context).load(jsonObject.optString("addURL")).into(swipeCardUserImage);
        }
        return view;
    }


    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        int count;

        public ViewPagerAdapter(FragmentManager fm, int count) {
            super(fm);
            this.count = count;
        }

        @Override
        public Fragment getItem(int position) {
            return TitlePagerFragment.newInstance("", "", position);
        }

        @Override
        public int getCount() {
            return count;
        }
    }


}
