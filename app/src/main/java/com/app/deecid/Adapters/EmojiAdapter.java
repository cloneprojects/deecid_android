package com.app.deecid.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.deecid.Activity.ChatActivity;
import com.app.deecid.R;

/**
 * Created by yuvaraj on 5/12/17.
 */

public class EmojiAdapter extends RecyclerView.Adapter<EmojiAdapter.MyViewHolder> {
    Context context;

    public EmojiAdapter(Context context) {
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.emojiindicator, parent, false);
        return new EmojiAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (position == 0) {
            holder.emojiIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.heart_sel_indicator));
        } else if (position==1)
        {
            holder.emojiIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.heart_un_selc_indicator));

        } else if (position==2)
        {
            holder.emojiIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_surprised_grey));

        }  else if (position==3)
        {
            holder.emojiIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_angry_grey));
        }

        holder.emojiIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatActivity.viewPager.setCurrentItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView emojiIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            emojiIcon = (ImageView) itemView.findViewById(R.id.emojiIcon);
        }
    }
}
