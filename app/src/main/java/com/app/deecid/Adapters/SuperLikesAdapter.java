package com.app.deecid.Adapters;

import android.app.Activity;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.deecid.Helper.AppSettings;
import com.app.deecid.Helper.UrlHelper;
import com.app.deecid.R;
import com.app.deecid.Volley.ApiCall;
import com.app.deecid.Volley.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * Created by admin on 01-06-2018.
 */

public class SuperLikesAdapter extends RecyclerView.Adapter<SuperLikesAdapter.MyViewHolder> {
    private static final String TAG = SuperLikesAdapter.class.getSimpleName();
    private Activity mContext;
    private JSONArray likedList;
    private AppSettings appSettings;

    public SuperLikesAdapter(Activity context, JSONArray likedList) {
        this.mContext = context;
        this.likedList = likedList;
        appSettings = new AppSettings(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.items_super_likes, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final JSONObject jsonObject = likedList.optJSONObject(holder.getAdapterPosition());
        Log.e(TAG, "onBindViewHolder: " + jsonObject);

        String name = jsonObject.optString("userName");
        String url = jsonObject.optString("displayPic");
        String age = jsonObject.optString("age");
        String jobTitle = jsonObject.optString("jobTitle");
        String company = jsonObject.optString("company");


        if (!jsonObject.optString("dontShowAge").equals("1")) {
            holder.userName.setText(new StringBuilder().append(name).append(", ").append(age));
        } else {
            holder.userName.setText(name);
        }

        //holder.userName.setText(new StringBuilder().append(name).append(", ").append(age));
        if (jobTitle != null) {
            holder.userDetails.setText(jobTitle);
        } else {
            holder.userDetails.setVisibility(View.GONE);
        }
        Glide.with(mContext).load(url).placeholder(R.drawable.pl_lis).into(holder.userImage);

        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSwipe(jsonObject.optString("id"), "like", holder.getAdapterPosition());
            }
        });

        holder.superlikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSwipe(jsonObject.optString("id"), "superlike", holder.getAdapterPosition());
            }
        });

        holder.nopeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSwipe(jsonObject.optString("id"), "nope", holder.getAdapterPosition());
            }
        });
    }

    private void updateSwipe(String receiverId, final String swipeType, final int position) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("senderID", appSettings.getUserId());
            jsonObject.put("receiverID", receiverId);
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("swipeType", swipeType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiCall.PostMethodNoProgress(mContext, UrlHelper.SWIPE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSwipeSuccess: " + response);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    likedList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return likedList.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView userImage;
        private TextView userName;
        private TextView userDetails;
        private RelativeLayout nopeButton;
        private RelativeLayout superlikeButton;
        private RelativeLayout likeButton;


        MyViewHolder(View itemView) {
            super(itemView);
            userImage = itemView.findViewById(R.id.userImage);
            userName = itemView.findViewById(R.id.userName);
            userDetails = itemView.findViewById(R.id.userDetails);
            nopeButton = itemView.findViewById(R.id.nopeButton);
            superlikeButton = itemView.findViewById(R.id.superlikeButton);
            likeButton = itemView.findViewById(R.id.likeButton);

        }
    }
}