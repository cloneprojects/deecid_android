package com.app.deecid.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.deecid.R;

import java.util.ArrayList;

/**
 * Created by user on 23-11-2017.
 */

public class CountryCodeAdapter extends BaseAdapter {
    Context context;

    public CountryCodeAdapter(Context context, ArrayList<String> phone_code, ArrayList<String> country_name) {
        this.context = context;
        this.phone_code = phone_code;
        this.country_name = country_name;
    }


    ArrayList<String> phone_code = new ArrayList<>();
    ArrayList<String> country_name = new ArrayList<>();

    @Override
    public int getCount() {
        return phone_code.size();
    }

    @Override
    public Object getItem(int i) {
        return phone_code.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View row = null;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            row = inflater.inflate(R.layout.spinner_text_view, viewGroup,
                    false);
        } else {
            row = convertView;
        }



        TextView name = (TextView) row.findViewById(R.id.countryName);
        TextView code = (TextView) row.findViewById(R.id.countryCode);
        name.setText(country_name.get(i));
        code.setText("("+phone_code.get(i)+")");
        return row;
    }
}
