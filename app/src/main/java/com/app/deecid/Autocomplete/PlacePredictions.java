package com.app.deecid.Autocomplete;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Kyra on 1/11/2016.
 */
public class PlacePredictions implements Serializable{

    public String strLatitude = "";
    public String strLongitude = "";
    public String strLatLng = "";
    public String strAddress = "";

    public ArrayList<PlaceAutoComplete> getPlaces() {
        return predictions;
    }

    public void setPlaces(ArrayList<PlaceAutoComplete> places) {
        this.predictions = places;
    }

    private ArrayList<PlaceAutoComplete> predictions;

}
